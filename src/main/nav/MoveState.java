package main.nav;

import battlecode.common.Direction;

/**
 * Class for things that are modified by Mover (but are not necessarily required by all things).
 */
public class MoveState {
    // For Bugger - the desired direction to go, i.e. here.directionTo(to)
    public Direction desiredDir;

    // For Mover - the damage amount of the move last returned
    public float dmg;
}
