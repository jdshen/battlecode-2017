package main.nav;

import battlecode.common.*;
import main.controllers.Controller;
import main.micro.Avoid;
import main.micro.DamageCache;
import main.micro.Dodge;
import main.util.Math2;
import main.util.OtherGameConstants;

public strictfp class Mover {
    // TODO reduce to 15
    // degrees to use for move towards
    public static final float DEGREES_TOWARDS = 45;
    // scout/gardener/archon try very hard to dodge, others will accept some damage. lumberjack will accept more.
    public final float ACCEPTABLE_DMG = getAcceptableDamage(Controller.c.type);

    // Optional paramter/return information
    public MoveState state = new MoveState();

    // whether to try to dodge bullets and avoid enemies when moving. false for Bugger.
    private final boolean dodge;
    private final Dodge dodger = new Dodge();

    private final boolean avoid;
    private final Avoid avoider = new Avoid();
    private final DamageCache cache = new DamageCache();

    public static float getAcceptableDamage(RobotType type) {
        switch(type) {
            case ARCHON:
            case GARDENER:
            case SCOUT:
                return OtherGameConstants.DELTA;
            case LUMBERJACK:
                return 2.5f; // only take into account bullets, since lumberjacks dont avoid anyways.
            case SOLDIER:
            case TANK:
                return 0.75f;
        }

        return 0.75f;
    }

    public Mover() {
        this(false, false);
    }

    public Mover(boolean dodge) {
        this(dodge, true);
    }

    public Mover(boolean dodge, boolean avoid) {
        this.dodge = dodge;
        this.avoid = avoid;
    }

    public void update() {
        if (dodge) {
            dodger.update();
        }

        if (avoid) {
            avoider.update();
        }

        if (dodge || avoid) {
            cache.update();
        }
    }

    // only tests dodge + avoid, for bytecode reasons
    public boolean canMove(MapLocation loc) {
        float dmg = 0;
        if (dodge) {
            dmg += dodger.damageAtLoc(loc);
        }

        if (avoid) {
            dmg += avoider.damageAtLoc(loc);
        }

        if (dodge || avoid) {
            cache.store(loc, dmg);
        }

        state.dmg = dmg;
        Controller.crc.setIndicatorDot(loc, 255, 255 - (int) (dmg * 25), 255 - (int) (dmg * 25));
        return dmg < OtherGameConstants.DELTA;
    }

    public Move moveTowardsTank(MapLocation to) {
        // localize
        RobotController rc = Controller.crc;
        Controller c = Controller.c;
        float stride = c.stride;

        MapLocation here = rc.getLocation();
        Direction desiredDir = Math2.directionTo(here, to);
        if (here.distanceTo(to) <= stride) {
            if (rc.canMove(to) && canMove(to)) {
                state.desiredDir = desiredDir;
                return new Move(here, to);
            }
        }

        state.desiredDir = desiredDir;

        // TODO do some lookaheads to figure out when we can strafe
        TreeInfo[] dt = Controller.crc.senseNearbyTrees(here.add(desiredDir, stride), c.bodyRadius, c.team);
        TreeInfo[] dn = Controller.crc.senseNearbyTrees(here.add(desiredDir, stride), c.bodyRadius, Team.NEUTRAL);
        if (rc.canMove(desiredDir) && dt.length == 0 && dn.length == 0 && canMove(here.add(desiredDir, stride))) {
            return new Move(desiredDir, stride);
        }

        // localize
        float DEGREES = Mover.DEGREES_TOWARDS;

        Direction l = desiredDir.rotateLeftDegrees(DEGREES);

        TreeInfo[] lt = Controller.crc.senseNearbyTrees(here.add(l, stride), c.bodyRadius, c.team);
        TreeInfo[] ln = Controller.crc.senseNearbyTrees(here.add(l, stride), c.bodyRadius, Team.NEUTRAL);
        if (rc.canMove(l) && lt.length == 0 && ln.length == 0 && canMove(here.add(l, stride))) {
            return new Move(l, stride);
        }

        Direction r = desiredDir.rotateRightDegrees(DEGREES);
        TreeInfo[] rt = Controller.crc.senseNearbyTrees(here.add(r, stride), c.bodyRadius, c.team);
        TreeInfo[] rn = Controller.crc.senseNearbyTrees(here.add(r, stride), c.bodyRadius, Team.NEUTRAL);
        if (rc.canMove(r) && rt.length == 0 && rn.length == 0 && canMove(here.add(r, stride))) {
            return new Move(r, stride);
        }

        if (rc.canMove(desiredDir) && dt.length == 0 && canMove(here.add(desiredDir, stride))) {
            return new Move(desiredDir, stride);
        }

        if (rc.canMove(l) && lt.length == 0 && canMove(here.add(l, stride))) {
            return new Move(l, stride);
        }

        if (rc.canMove(r) && rt.length == 0 && canMove(here.add(r, stride))) {
            return new Move(r, stride);
        }

        return null;
    }

    public Move moveTowards(MapLocation to) {
        // localize
        RobotController rc = Controller.crc;
        Controller c = Controller.c;
        float stride = c.stride;

        if (c.type == RobotType.TANK) {
            return moveTowardsTank(to);
        }

        MapLocation here = rc.getLocation();
        Direction desiredDir = Math2.directionTo(here, to);
        if (here.distanceTo(to) <= stride) {
            if (rc.canMove(to) && canMove(to)) {
                state.desiredDir = desiredDir;
                return new Move(here, to);
            }
        }

        state.desiredDir = desiredDir;

        // TODO do some lookaheads to figure out when we can strafe
        if (rc.canMove(desiredDir) && canMove(here.add(desiredDir, stride))) {
            return new Move(desiredDir, stride);
        }

        // localize
        float DEGREES = Mover.DEGREES_TOWARDS;

        Direction l = desiredDir.rotateLeftDegrees(DEGREES);
        if (rc.canMove(l) && canMove(here.add(l, stride))) {
            return new Move(l, stride);
        }

        Direction r = desiredDir.rotateRightDegrees(DEGREES);
        if (rc.canMove(r) && canMove(here.add(r, stride))) {
            return new Move(r, stride);
        }

        return null;
    }

    /**
     * Tries to move closest to "to" and dodge/avoid bullets
     */
    public Move dodgeTowards(MapLocation to) {
        // localize
        RobotController rc = Controller.crc;
        Controller c = Controller.c;
        float stride = c.stride;
        int cacheStart = cache.index;

        MapLocation here = rc.getLocation();
        Direction desiredDir = Math2.directionTo(here, to);
        if (here.distanceTo(to) <= stride) {
            if (rc.canMove(to) && canMove(to)) {
                return new Move(here, to);
            }
        }

        // TODO do some lookaheads to figure out when we can strafe
        if (rc.canMove(desiredDir) && canMove(here.add(desiredDir, stride))) {
            return new Move(desiredDir, stride);
        }

        // localize
        float DEGREES = Mover.DEGREES_TOWARDS;

        Direction l = desiredDir.rotateLeftDegrees(DEGREES);
        if (rc.canMove(l) && canMove(here.add(l, stride))) {
            return new Move(l, stride);
        }

        Direction r = desiredDir.rotateRightDegrees(DEGREES);
        if (rc.canMove(r) && canMove(here.add(r, stride))) {
            return new Move(r, stride);
        }

        if (dodge || avoid) {
            int index = cache.minDamageIndex(cacheStart);
            if (index >= 0 && cache.dmg[index] <= ACCEPTABLE_DMG) {
                return new Move(here, cache.locs[index]);
            }

            if (canMove(here)) {
                return new Move(here, here);
            }

            float stayDmg = state.dmg;
            if (stayDmg < ACCEPTABLE_DMG) {
                return new Move(here, here);
            }

            Move move = moveAway(to);
            if (move == null  && index >= 0) {
                return new Move(here, cache.locs[index]);
            }

            if (move != null && index < 0) {
                return move;
            }

            if (index >= 0 && cache.dmg[index] < stayDmg) {
                if (index >= 0 && cache.dmg[index] < state.dmg) {
                    return new Move(here, cache.locs[index]);
                } else {
                    return move;
                }
            } else {
                if (stayDmg < state.dmg) {
                    return new Move(here, here);
                } else {
                    return move;
                }
            }

        }

        return null;
    }

    public Move moveAway(MapLocation from){
        // localize
        RobotController rc = Controller.crc;
        Controller c = Controller.c;
        float stride = c.stride;
        int cacheStart = cache.index;

        MapLocation here = rc.getLocation();
        Direction desiredDir = Math2.directionTo(from, here);

        // TODO do some lookaheads to figure out when we can strafe
        if (rc.canMove(desiredDir) && canMove(here.add(desiredDir, stride))) {
            return new Move(desiredDir, stride);
        }

        // localize
        float DEGREES = Mover.DEGREES_TOWARDS;

        Direction l = desiredDir.rotateLeftDegrees(DEGREES);
        if (rc.canMove(l) && canMove(here.add(l, stride))) {
            return new Move(l, stride);
        }

        Direction r = desiredDir.rotateRightDegrees(DEGREES);
        if (rc.canMove(r) && canMove(here.add(r, stride))){
            return new Move(r, stride);
        }

        // move perpendicular if possible
        Direction ll = l.rotateLeftDegrees(DEGREES);
        if (rc.canMove(ll) && canMove(here.add(ll, stride))) {
            return new Move(ll, stride);
        }

        Direction rr = r.rotateRightDegrees(DEGREES);
        if (rc.canMove(rr) && canMove(here.add(rr, stride))){
            return new Move(rr, stride);
        }

        if (dodge || avoid) {
            // TODO also try other locs to dodge
            int index = cache.minDamageIndex(cacheStart);
            if (index >= 0) {
                state.dmg = cache.dmg[index];
                return new Move(here, cache.locs[index]);
            }
        }

        return null;
    }

    // Moves adjacent to info if possible
    public Move follow(RobotInfo info) {
        RobotController rc = Controller.crc;
        MapLocation to = info.location;
        MapLocation here = rc.getLocation();

        float radius = info.getRadius() + Controller.c.bodyRadius + OtherGameConstants.EPSILON;
        float adjMove = here.distanceTo(to) - radius;
        if (adjMove < Controller.c.stride) {
            Direction toHere = to.directionTo(here);
            MapLocation desired = to.add(toHere, radius);
            if (rc.canMove(desired) && canMove(desired)) {
                return new Move(here, desired);
            }
        }

        // otherwise just move towards
        return moveTowards(to);
    }
}
