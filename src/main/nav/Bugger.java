package main.nav;

import battlecode.common.*;
import main.controllers.Controller;
import main.util.FastLocDirSet;
import main.util.OtherGameConstants;

public strictfp class Bugger {
    public MapLocation start;
    public MapLocation from;
    public MapLocation here;
    public MapLocation to;

    public boolean hugging;
    public boolean hugLeft;

    public Direction hugDir;
    public FastLocDirSet seen;
    public boolean recursed;
    public boolean bugging;

    private Mover mover;

    // Used for hugging. Note: MUST CHANGE HUG LOOP ALSO (hardcoded at 8 directions)
    public static final float DEGREES = 45;

    public Bugger(Mover m) {
        mover = m;
        seen = new FastLocDirSet();
        bugging = false;
    }

    public Bugger() {
        this(new Mover());
    }

    public void endBug() {
        bugging = false;
    }

    public void startBug(MapLocation loc) {
        RobotController rc = Controller.crc;
        start = rc.getLocation();
        from = rc.getLocation();
        to = loc;
        hugging = false;
        hugLeft = true;
        bugging = true;
    }

    // Something went wrong during, restart the bugging.
    private void restart(MapLocation loc) {
        from = Controller.crc.getLocation();
        to = loc;
        hugging = false;
        hugLeft = true;
        seen.clear();
    }

    public Move bug() throws GameActionException {
        RobotController rc = Controller.crc;
        rc.setIndicatorDot(to, 0, 255, 0);
        here = rc.getLocation();

        // localize
        MapLocation here = this.here;
        MapLocation to = this.to;

        if (here.distanceTo(to) <= Controller.c.stride - OtherGameConstants.EPSILON) {
            if (rc.canMove(to)) {
                return new Move(here, to);
            }
        }

        // desiredDir if possible, else left, else right. Makes dist smaller
        Move bestMove = mover.moveTowards(to);

        if (hugging) {
            // been here before, restart bugging
            if (seen.contains(here, hugDir) ) {
                rc.setIndicatorDot(here, 255, 255, 0);
                hugging = false;
            }
        }

        if (hugging) {
            if (bestMove != null) {
                float dist = from.distanceTo(to);
                if (bestMove.add(here).distanceTo(to) < dist - OtherGameConstants.DELTA) {
                    hugging = false;
                    return bestMove;
                }
            }

            seen.add(here, hugDir);
            return hug(hugLeft);
        } else {
            if (bestMove != null) {
                return bestMove;
            }

            // start hugging
            seen.clear();
            hugging = true;
            from = here;
            hugDir = mover.state.desiredDir;
            recursed = false;

            Move left = hug(true);
            Direction leftHug = this.hugDir;
            Move right = hug(false);
            Direction rightHug = this.hugDir;
            if (left == null) {
                hugLeft = false;
                this.hugDir = rightHug;
                return right;
            }

            if (right == null) {
                hugLeft = true;
                this.hugDir = leftHug;
                return left;
            }

            if (to.distanceTo(left.add(here)) < to.distanceTo(right.add(here))) {
                hugLeft = true;
                this.hugDir = leftHug;
                return left;
            } else {
                hugLeft = false;
                this.hugDir = rightHug;
                return right;
            }
        }
    }

    private Move handleOffMap() throws GameActionException {
        if (!recursed) {
            seen.clear();
            hugLeft = !hugLeft;
            recursed = true;
            return hug(hugLeft);
        } else {
            //something went wrong, reset
            restart(to);
            return null;
        }
    }

    private Move hugScout(boolean hugLeft) throws GameActionException {
        MapLocation here = this.here;
        RobotController rc = Controller.crc;
        float bodyRadius = Controller.c.bodyRadius;
        float DEGREES = Bugger.DEGREES;
        float stride = Controller.c.stride;
        rc.setIndicatorDot(here.add(hugDir, stride), 0, 255, 255);

        if (hugLeft) {
            Direction dir = hugDir.rotateLeftDegrees(DEGREES);
            MapLocation loc = here.add(dir, stride);
            for (int i = 8; --i >= 0; ) {
                if (rc.canMove(dir, stride) && mover.canMove(loc)) {
                    hugDir = here.add(dir).directionTo(here.add(dir.rotateRightDegrees(DEGREES)));
                    return new Move(dir, stride);
                }

                if (!rc.onTheMap(loc, bodyRadius)) {
                    return handleOffMap();
                }

                dir = dir.rotateLeftDegrees(DEGREES);
                loc = here.add(dir, stride);
            }
        } else {
            Direction dir = hugDir.rotateRightDegrees(DEGREES);
            MapLocation loc = here.add(dir, stride);
            for (int i = 8; --i >= 0; ) {
                if (rc.canMove(dir, stride) && mover.canMove(loc)) {
                    hugDir = here.add(dir).directionTo(here.add(dir.rotateLeftDegrees(DEGREES)));
                    return new Move(dir, stride);
                }

                if (!rc.onTheMap(loc, bodyRadius)) {
                    return handleOffMap();
                }

                dir = dir.rotateRightDegrees(DEGREES);
                loc = here.add(dir, stride);
            }
        }

        return null;
    }

    private Move hugTank(boolean hugLeft) throws GameActionException {
        MapLocation here = this.here;
        RobotController rc = Controller.crc;
        float bodyRadius = Controller.c.bodyRadius;
        Team team = Controller.c.team;
        float DEGREES = Bugger.DEGREES;
        float stride = Controller.c.stride;
        rc.setIndicatorDot(here.add(hugDir, stride), 0, 255, 255);

        if (hugLeft) {
            Direction dir = hugDir.rotateLeftDegrees(DEGREES);
            MapLocation loc = here.add(dir, stride);
            for (int i = 8; --i >= 0; ) {
                if (rc.canMove(dir, stride) && rc.senseNearbyTrees(loc, bodyRadius, team).length == 0) {
                    hugDir = here.add(dir).directionTo(here.add(dir.rotateRightDegrees(DEGREES)));
                    return new Move(dir, stride);
                }

                if (!rc.onTheMap(loc, bodyRadius)) {
                    return handleOffMap();
                }

                dir = dir.rotateLeftDegrees(DEGREES);
                loc = here.add(dir, stride);
            }
        } else {
            Direction dir = hugDir.rotateRightDegrees(DEGREES);
            MapLocation loc = here.add(dir, stride);
            for (int i = 8; --i >= 0; ) {
                if (rc.canMove(dir, stride) && rc.senseNearbyTrees(loc, bodyRadius, team).length == 0) {
                    hugDir = here.add(dir).directionTo(here.add(dir.rotateLeftDegrees(DEGREES)));
                    return new Move(dir, stride);
                }

                if (!rc.onTheMap(loc, bodyRadius)) {
                    return handleOffMap();
                }

                dir = dir.rotateRightDegrees(DEGREES);
                loc = here.add(dir, stride);
            }
        }

        return null;
    }

    private Move hug(boolean hugLeft) throws GameActionException {
        if (Controller.c.type == RobotType.TANK) {
            return hugTank(hugLeft);
        }

        if (Controller.c.type == RobotType.SCOUT) {
            return hugScout(hugLeft);
        }
        MapLocation here = this.here;
        RobotController rc = Controller.crc;
        float bodyRadius = Controller.c.bodyRadius;
        float DEGREES = Bugger.DEGREES;
        float stride = Controller.c.stride;
        rc.setIndicatorDot(here.add(hugDir, stride), 0, 255, 255);

        if (hugLeft) {
            Direction dir = hugDir.rotateLeftDegrees(DEGREES);
            MapLocation loc = here.add(dir, stride);
            for (int i = 8; --i >= 0; ) {
                if (rc.canMove(dir, stride)) {
                    hugDir = here.add(dir).directionTo(here.add(dir.rotateRightDegrees(DEGREES)));
                    return new Move(dir, stride);
                }

                if (!rc.onTheMap(loc, bodyRadius)) {
                    return handleOffMap();
                }

                dir = dir.rotateLeftDegrees(DEGREES);
                loc = here.add(dir, stride);
            }
        } else {
            Direction dir = hugDir.rotateRightDegrees(DEGREES);
            MapLocation loc = here.add(dir, stride);
            for (int i = 8; --i >= 0; ) {
                if (rc.canMove(dir, stride)) {
                    hugDir = here.add(dir).directionTo(here.add(dir.rotateLeftDegrees(DEGREES)));
                    return new Move(dir, stride);
                }

                if (!rc.onTheMap(loc, bodyRadius)) {
                    return handleOffMap();
                }

                dir = dir.rotateRightDegrees(DEGREES);
                loc = here.add(dir, stride);
            }
        }

        return null;
    }
}