package main.nav;

import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;

public strictfp class Move {
    public Direction dir;
    public float dist;

    public Move(Direction dir, float dist) {
        if (dir == null) {
            dir = Direction.NORTH;
        }
        this.dir = dir;
        this.dist = dist;
    }

    public Move(MapLocation here, MapLocation loc) {
        this.dir = here.directionTo(loc);
        if (dir == null) {
            dir = Direction.getNorth();
        }

        dist = here.distanceTo(loc);
    }

    public MapLocation add(MapLocation loc) {
        return loc.add(dir, dist);
    }

    public boolean canMove(RobotController rc) {
        return rc.canMove(dir, dist);
    }

    public void make(RobotController rc) throws GameActionException {
        if (rc.canMove(dir, dist)) {
            rc.move(dir, dist);
        }
    }

    @Override
    public String toString() {
        return "Move{" +
            "dir=" + dir +
            ", dist=" + dist +
            '}';
    }
}
