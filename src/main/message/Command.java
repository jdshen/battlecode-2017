package main.message;

import battlecode.common.MapLocation;
import main.controllers.Controller;

public class Command {
    public final MapLocation location;
    public final CommandType command;
    public final int round;

    public Command(MapLocation location, CommandType command, int round) {
        this.location = location;
        this.command = command;
        this.round = round;
    }

    public Command(MapLocation location, CommandType command) {
        this(location, command, Controller.crc.getRoundNum());
    }

    public boolean isFresh() {
        return round + 5 >= Controller.crc.getRoundNum();
    }
}
