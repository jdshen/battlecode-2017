package main.message;

import battlecode.common.*;
import main.controllers.Controller;
import main.helper.BuildType;
import main.micro.AttackInfo;
import main.micro.AttackType;
import main.util.OtherGameConstants;

public class CommandManager {
    private final int primaryChannel = Controller.c.type.ordinal() * MessagingConstants.NUM_CMD_GROUPS +
        Controller.crc.getID() % MessagingConstants.NUM_CMD_GROUPS + MessagingConstants.CMD_OFFSET;

    private final int offsetX =
        (int)Controller.crc.getInitialArchonLocations(Controller.c.team)[0].x - OtherGameConstants.MAX_MAP_SIDE;
    private final int offsetY =
        (int)Controller.crc.getInitialArchonLocations(Controller.c.team)[0].y - OtherGameConstants.MAX_MAP_SIDE;

    private static final int HASH = OtherGameConstants.MAX_MAP_SIDE * 2 + 1;
    private static final int HASH_SQ = HASH * HASH;


    public Command readCommand() throws GameActionException {
        return readCommand(primaryChannel);
    }

    public Command readCommand(int channel) throws GameActionException {
        return getCommand(Controller.crc.readBroadcast(channel));
    }

    // Returns the resulting command
    public Command setCommand(Command c, RobotType unit, int group) throws GameActionException {
        RobotController rc = Controller.crc;
        int channel = unit.ordinal() * MessagingConstants.NUM_CMD_GROUPS + group + MessagingConstants.CMD_OFFSET;
        int prevMsg = rc.readBroadcast(channel);
        Command prev = getCommand(prevMsg);
        int round = rc.getRoundNum();
        if (prev == null || c.command.ordinal() < prev.command.ordinal() || prev.round + 5 < round) {
            rc.broadcast(channel, getMessage(c));
            return c;
        }

        if (c.command.ordinal() == prev.command.ordinal() &&
                c.location.isWithinDistance(prev.location, OtherGameConstants.STD_SIGHT_RADIUS)) {
            rc.broadcast(channel, getRefresh(prevMsg, prev.round, round));
            return new Command(prev.location, prev.command, round);
        }

        return prev;
    }

    // Returns the resulting command
    public Command setCommand(Command c) throws GameActionException {
        RobotController rc = Controller.crc;
        int prevMsg = rc.readBroadcast(primaryChannel);
        Command prev = getCommand(prevMsg);
        int round = rc.getRoundNum();
        if (prev == null || c.command.ordinal() < prev.command.ordinal() || prev.round + 5 < round) {
            rc.broadcast(primaryChannel, getMessage(c));
            return c;
        }

        if (c.command.ordinal() == prev.command.ordinal() &&
                c.location.isWithinDistance(prev.location, OtherGameConstants.STD_SIGHT_RADIUS)) {
            rc.broadcast(primaryChannel, getRefresh(prevMsg, prev.round, round));
            return new Command(prev.location, prev.command, round);
        }

        return prev;
    }

    public void bulkSetCommand(Command c, RobotType unit) throws GameActionException {
        int msg = getMessage(c);
        int channel = unit.ordinal() * MessagingConstants.NUM_CMD_GROUPS + MessagingConstants.CMD_OFFSET;

        // localize
        RobotController rc = Controller.crc;
        int typeOrd = c.command.ordinal();
        MapLocation loc = c.location;
        float sr = OtherGameConstants.STD_SIGHT_RADIUS;
        int end = channel + MessagingConstants.NUM_CMD_GROUPS;

        for (int i = channel; i < end; i++) {
            int prevMsg = rc.readBroadcast(i);
            Command prev = getCommand(prevMsg);
            int round = rc.getRoundNum();
            if (prev == null || typeOrd < prev.command.ordinal() || prev.round + 5 < round) {
                rc.broadcast(i, msg);
            } else if (typeOrd == prev.command.ordinal() && loc.isWithinDistance(prev.location, sr)) {
                rc.broadcast(i, getRefresh(prevMsg, prev.round, round));
            }
        }
    }

    public void bulkDeleteCommand(Command c, RobotType unit) throws GameActionException {
        int channel = unit.ordinal() * MessagingConstants.NUM_CMD_GROUPS + MessagingConstants.CMD_OFFSET;

        // localize
        RobotController rc = Controller.crc;
        CommandType type = c.command;
        MapLocation loc = c.location;
        float stride = Controller.c.stride;
        int end = channel + MessagingConstants.NUM_CMD_GROUPS;

        for (int i = channel; i < end; i++) {
            int prevMsg = rc.readBroadcast(i);
            Command prev = getCommand(prevMsg);
            if (prev != null && type == prev.command && loc.isWithinDistance(prev.location, stride)) {
                rc.broadcast(i, 0);
            }
        }
    }

    public void sendAttackCommand(MapLocation loc) throws GameActionException {
        Command command = new Command(loc, CommandType.ATTACK, Controller.crc.getRoundNum());
        bulkSetCommand(command, RobotType.SOLDIER);
        bulkSetCommand(command, RobotType.TANK);
        Command lumber = new Command(loc, CommandType.LUMBER, Controller.crc.getRoundNum());
        bulkSetCommand(lumber, RobotType.LUMBERJACK);
    }

    public void deleteAttackCommand(MapLocation loc) throws GameActionException {
        Command command = new Command(loc, CommandType.ATTACK, Controller.crc.getRoundNum());
        bulkDeleteCommand(command, RobotType.SOLDIER);
        bulkDeleteCommand(command, RobotType.TANK);
        Command lumber = new Command(loc, CommandType.LUMBER, Controller.crc.getRoundNum());
        bulkDeleteCommand(lumber, RobotType.LUMBERJACK);
    }

    private int getMessage(Command c) {
        int y = (int) c.location.y - offsetY;
        int x = (int) c.location.x - offsetX;
        return ((c.command.ordinal() * OtherGameConstants.MAX_ROUND+ c.round) * HASH + x) * HASH + y + 1;
    }

    private int getRefresh(int msg, int oldRound, int newRound) {
        return msg + HASH_SQ * (newRound - oldRound);
    }

    private Command getCommand(int msg) {
        if (msg == 0) {
            return null;
        }

        int HASH = CommandManager.HASH;
        --msg;
        int y = (msg % HASH) + offsetY;
        msg /= HASH;
        int x = (msg % HASH) + offsetX;
        msg /= HASH;
        int round = msg % OtherGameConstants.MAX_ROUND;
        CommandType type = CommandType.values()[msg / OtherGameConstants.MAX_ROUND];
        return new Command(new MapLocation(x, y), type, round);
    }

    //===============================================================================
    // Section for build counts
    //===============================================================================

    public void build(BuildType type) throws GameActionException {
        int channel = MessagingConstants.BUILD_COUNTS_OFFSET + type.ordinal();
        Controller.crc.broadcast(channel, Controller.crc.readBroadcast(channel) + 1);
    }

    public int getBuiltCount(BuildType type) throws GameActionException {
        return Controller.crc.readBroadcast(MessagingConstants.BUILD_COUNTS_OFFSET + type.ordinal());
    }

    public int[] getBuiltCounts() throws GameActionException {
        int channelOffset = MessagingConstants.BUILD_COUNTS_OFFSET;
        int[] counts = new int[OtherGameConstants.NUM_BUILD_TYPES];
        RobotController rc = Controller.crc;
        for (int i = counts.length; --i >= 0;) {
            counts[i] = rc.readBroadcast(channelOffset + i);
        }
        return counts;
    }

    //===============================================================================
    // Section for unit counts
    //===============================================================================

    private final int countChannel = MessagingConstants.UNIT_COUNTS_OFFSET + Controller.c.type.ordinal();
    private int[] lastUnitCounts = null;

    /**
     * should be called by all robots every turn. For tracking unit counts
     * @throws GameActionException
     */
    public void updateCount() throws GameActionException {
        Controller.crc.broadcast(countChannel, Controller.crc.readBroadcast(countChannel) + 1);
    }

    /**
     * track unit counts. should be called every round to accurately do it.
     * @return unit counts array (index = type ordinal), null if not ready
     */
    public int[] getUnitCounts() throws GameActionException {
        int numBots = OtherGameConstants.NUM_ROBOT_TYPES;
        int channelOffset = MessagingConstants.UNIT_COUNTS_OFFSET;
        int[] newUnitCounts = new int[numBots];
        RobotController rc = Controller.crc;
        if (lastUnitCounts == null) {
            for (int i = numBots; --i >= 0;) {
                newUnitCounts[i] = rc.readBroadcast(channelOffset + i);
            }
            lastUnitCounts = newUnitCounts;
            return null;
        } else {
            int[] trueCounts = new int[numBots];
            for (int i = numBots; --i >= 0;) {
                int newCount = rc.readBroadcast(channelOffset + i);
                newUnitCounts[i] = newCount;
                trueCounts[i] = newCount - lastUnitCounts[i];
            }
            lastUnitCounts = newUnitCounts;
            return trueCounts;
        }
    }

    //===============================================================================
    // Section for deques
    //===============================================================================

    public int buildTail = -1;

    public int getMessage(MapLocation loc) {
        int y = (int) loc.y - offsetY;
        int x = (int) loc.x - offsetX;
        return x * HASH + y + 1;
    }

    public void addBuildtoDeque(MapLocation loc) throws GameActionException {
        int head = Controller.crc.readBroadcast(MessagingConstants.BUILD_LOCS_OFFSET);
        Controller.crc.broadcast(MessagingConstants.BUILD_LOCS_OFFSET + head + 1, getMessage(loc));
        Controller.crc.broadcast(MessagingConstants.BUILD_LOCS_OFFSET, (head + 1) % MessagingConstants.DEQUE_SIZE);
    }

    public void updateBuildTail() throws GameActionException {
        buildTail = Controller.crc.readBroadcast(MessagingConstants.BUILD_LOCS_OFFSET);
    }

    public MapLocation[] getBuildDeque() throws GameActionException {
        if (buildTail < 0) {
            return null;
        }

        RobotController rc = Controller.crc;
        int head = rc.readBroadcast(MessagingConstants.BUILD_LOCS_OFFSET);
        MapLocation[] locs = new MapLocation[head - buildTail];
        int tail = MessagingConstants.BUILD_LOCS_OFFSET + buildTail + 1;
        head += MessagingConstants.BUILD_LOCS_OFFSET + 1;
        int offsetX = this.offsetX;
        int offsetY = this.offsetY;
        int HASH = CommandManager.HASH;
        for (int i = tail; i < head; i++) {
            int msg = rc.readBroadcast(i);
            --msg;
            locs[i - tail] = new MapLocation(msg / HASH + offsetX, msg % HASH + offsetY);
        }

        return locs;
    }

    // Current tank positions
    public int tankTail = -1;

    public void addTanktoDeque(MapLocation loc) throws GameActionException {
        int head = Controller.crc.readBroadcast(MessagingConstants.TANK_LOCS_OFFSET);
        Controller.crc.broadcast(MessagingConstants.TANK_LOCS_OFFSET + head + 1, getMessage(loc));
        Controller.crc.broadcast(MessagingConstants.TANK_LOCS_OFFSET, (head + 1) % MessagingConstants.DEQUE_SIZE);
    }

    public void updateTankTail() throws GameActionException {
        tankTail = Controller.crc.readBroadcast(MessagingConstants.TANK_LOCS_OFFSET);
    }

    public MapLocation[] getTankDeque() throws GameActionException {
        if (tankTail < 0) {
            return null;
        }

        RobotController rc = Controller.crc;
        int head = rc.readBroadcast(MessagingConstants.TANK_LOCS_OFFSET);
        int tail = tankTail;
        if (head < tail) {
            head += MessagingConstants.DEQUE_SIZE;
        }
        MapLocation[] locs = new MapLocation[head - tail];
        int offsetX = this.offsetX;
        int offsetY = this.offsetY;
        int HASH = CommandManager.HASH;
        int length = locs.length;
        for (int i = 0; i < length; i++) {
            int msg = rc.readBroadcast((i + tail) % MessagingConstants.DEQUE_SIZE + MessagingConstants.TANK_LOCS_OFFSET + 1);
            --msg;
            locs[i] = new MapLocation(msg / HASH + offsetX + 0.5f, msg % HASH + offsetY + 0.5f);
            rc.setIndicatorDot(locs[i], 100, 100, 50);
        }

        return locs;
    }
}
