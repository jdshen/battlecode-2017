package main.message;

public enum CommandType {
    DEFEND,
    ATTACK,
    CLEAR,
    LUMBER, // ATTACK but for lumberjacks
    BUILD, // A gardener build location.
}
