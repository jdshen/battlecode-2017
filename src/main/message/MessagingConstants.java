package main.message;

public class MessagingConstants {
    public static final int CMD_OFFSET = 0;
    public static final int NUM_CMD_GROUPS = 3;
    // 3 x 10

    public static final int BUILD_LOCS_OFFSET = 50;
    // +100 -> 150

    public static final int TANK_LOCS_OFFSET = 200;
    // +100 -> 300


    public static final int BUILD_COUNTS_OFFSET = 980;
    // +7

    public static final int UNIT_COUNTS_OFFSET = 1020;
    // +6

    public static final int DEQUE_SIZE = 100;
}
