package main.controllers;

import battlecode.common.*;
import main.act.Donate;
import main.act.Shake;
import main.message.Command;
import main.nav.Move;
import main.util.OtherGameConstants;

public strictfp class SoldierController extends Controller {
    private final Shake shake = new Shake();
    private final Donate donate = new Donate();

    MapLocation goal;

    public SoldierController(RobotController rc) {
        super(rc);
    }

    @Override
    public void run() throws GameActionException {
        cmd.updateCount();
        m.update();
        donate.donate();
        shake.shake();

        boolean shouldHitArchon = false;
        int[] unitCounts = cmd.getUnitCounts();
        if (unitCounts != null) {
            shouldHitArchon = unitCounts[RobotType.SOLDIER.ordinal()] > 3 && unitCounts[RobotType.GARDENER.ordinal()] > 0;
        }

        Controller c = Controller.c;
        RobotController rc = this.rc;
        Team enemy = c.enemy;

        RobotInfo[] enemies = rc.senseNearbyRobots(-1, enemy);
        // send command
        if (enemies.length > 0 && enemies[0].type != RobotType.SCOUT) {
            Controller.cmd.sendAttackCommand(enemies[0].location);
        }

        updateDefaultTarget(enemies);

        // try to go towards closest enemy
        MapLocation target = findNonDefaultTarget(enemies);
        if (target == null) {
            target = goal;
        }

        Move move = getTargetMove();
        if (move != null) {
            atk.makeAttackMove(move, target, shouldHitArchon);
        }
    }

    public Move getTargetMove() throws GameActionException {
        MapLocation here = rc.getLocation();
        if (!bugger.bugging) {
            bugger.startBug(goal);
        }

        Move move = bugger.bug();
        if (move != null && Controller.m.canMove(move.add(here))) {
            return move;
        }

        bugger.endBug();

        move = m.dodgeTowards(goal);
        return move;
    }

    // return if you want to rebroadcast a location
    public void updateDefaultTarget(RobotInfo[] enemies) throws GameActionException {
        Controller c = Controller.c;
        RobotController rc = this.rc;
        Command command = Controller.cmd.readCommand();
        if (command != null && command.isFresh()) {
            goal = command.location;
            if (goal.isWithinDistance(rc.getLocation(), stride)) {
                Controller.cmd.deleteAttackCommand(goal);

                if (enemies.length > 0) {
                    goal = enemies[0].getLocation();
                    Controller.cmd.sendAttackCommand(goal);
                }

            }

            if (!bugger.bugging || bugger.to == null || !goal.isWithinDistance(bugger.to, stride)) {
                bugger.startBug(goal);
            }

            return;
        }

        if (goal == null || goal.isWithinDistance(rc.getLocation(), sr / 2)) {
            MapLocation[] archonLocs = rc.getInitialArchonLocations(enemy);
            goal = archonLocs[c.id % archonLocs.length];
            bugger.startBug(goal);
            Controller.cmd.sendAttackCommand(goal);
            return;
        }
    }

    public MapLocation findNonDefaultTarget(RobotInfo[] enemies) {
        MapLocation target = null;
        float distTo = OtherGameConstants.MAX_DIST;
        MapLocation here = rc.getLocation();

        TreeInfo[] enemyTrees = rc.senseNearbyTrees(-1, enemy);
        if (enemyTrees.length > 0) {
            target = enemyTrees[0].location;
            distTo = here.distanceTo(target);
        }

        if (enemies.length > 0) {
            MapLocation loc = enemies[0].location;
            float dist = here.distanceTo(loc);
            if (dist < distTo) {
                target = loc;
            }
        }

        return target;
    }


}