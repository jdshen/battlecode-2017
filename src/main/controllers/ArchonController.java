package main.controllers;

import battlecode.common.*;
import main.act.Donate;
import main.act.Hire;
import main.act.Shake;
import main.nav.Move;
import main.util.OtherGameConstants;

public strictfp class ArchonController extends Controller {
    private Hire hire = new Hire();
    private Donate donate = new Donate();
    private final Shake shake = new Shake();

    public ArchonController(RobotController rc) {
        super(rc);
    }

    public RobotInfo getNearbyGardener() {
        RobotInfo[] infos = rc.senseNearbyRobots(OtherGameConstants.PLANT_RADIUS + bodyRadius, team);
        int len = Math.min(infos.length, 16);
        for (int i = 0; i < len; i++) {
            RobotInfo info = infos[i];
            if (info.type == RobotType.GARDENER) {
                return info;
            }
        }
        return null;
    }

    public Direction getAwayFromWall(MapLocation here) throws GameActionException {
        Direction d = Direction.NORTH;
        float radius = c.sr - OtherGameConstants.EPSILON;
        int[] scores = new int[8];

        for (int i = 0; i < 8; i++) {
            MapLocation loc = here.add(d.rotateLeftDegrees(45 * i), radius);
            if (!rc.onTheMap(loc)) {
                scores[i] += 1;
                scores[(i + 1) % 8] += 1;
                scores[(i + 7) % 8] += 1;
            }
        }

        int bestScore = 0;
        int bestIdx = -1;
        for (int i = 0; i < 8; i++) {
            if (scores[i] > bestScore) {
                bestScore = scores[i];
                bestIdx = i;
            }
        }
        if (bestIdx >= 0) {
            return d.rotateLeftDegrees(45 * bestIdx).opposite();
        }
        return null;
    }

    @Override
    public void run() throws GameActionException {
        shake.shake();
        m.update();

        int[] unitCounts = cmd.getUnitCounts();
        if (rc.getRoundNum() <= 1 && unitCounts == null) {
            unitCounts = new int[OtherGameConstants.NUM_ROBOT_TYPES];
            unitCounts[RobotType.ARCHON.ordinal()] += rc.getInitialArchonLocations(team).length;
        }

        int[] buildCounts = cmd.getBuiltCounts();
        int trees = rc.getTreeCount();

        hire.hire(buildCounts, unitCounts, trees);
        cmd.updateCount();


        RobotInfo[] enemies = rc.senseNearbyRobots(-1, enemy);
        if (enemies.length > 0) {
            RobotInfo enemy = enemies[0];
            rc.setIndicatorDot(enemy.location, 0, 0, 0);
            Move move = Controller.m.moveAway(enemy.location);
            if (move != null) {
                move.make(rc);
            }
        }
        if (!rc.hasMoved()) {
            MapLocation here = rc.getLocation();
            Direction dir = getAwayFromWall(here);
            if (dir != null) {
                rc.setIndicatorDot(here.add(dir), 100, 100, 100);
                Move move = Controller.m.moveTowards(here.add(dir, 10));
                if (move != null) {
                    move.make(rc);
                }
            }
        }
        if (!rc.hasMoved()) {
            RobotInfo gardener = getNearbyGardener();
            if (gardener != null) {
                rc.setIndicatorDot(gardener.location, 255, 255, 255);
                Move move = Controller.m.moveAway(gardener.location);
                if (move != null) {
                    move.make(rc);
                }
            }
        }
        
        donate.donate();
    }
}