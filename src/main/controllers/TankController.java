package main.controllers;

import battlecode.common.*;
import main.act.Donate;
import main.act.Shake;
import main.message.Command;
import main.nav.Move;

public strictfp class TankController extends Controller {
    private final Shake shake = new Shake();
    private final Donate donate = new Donate();
    MapLocation targetLoc;

    public TankController(RobotController rc) {
        super(rc);
    }

    @Override
    public void run() throws GameActionException {
        donate.donate();
        cmd.updateCount();
        m.update();

        boolean shouldHitArchon = false;
        int[] unitCounts = cmd.getUnitCounts();
        if (unitCounts != null) {
            shouldHitArchon = unitCounts[RobotType.SOLDIER.ordinal()] > 3 && unitCounts[RobotType.GARDENER.ordinal()] > 0;
        }


        Controller c = Controller.c;
        RobotController rc = this.rc;
        Team enemy = c.enemy;

        Command command = Controller.cmd.readCommand();
        if (command != null && command.isFresh()) {
            targetLoc = command.location;
            if (bugger.to == null || !targetLoc.isWithinDistance(bugger.to, stride)) {
                bugger.startBug(targetLoc);
            }
        }

        if (targetLoc == null) {
            MapLocation[] archonLocs = rc.getInitialArchonLocations(enemy);
            targetLoc = archonLocs[c.id % archonLocs.length];
            bugger.startBug(targetLoc);
        }
        shake.shake();

        Move move = bugger.bug();
        if (move != null) {
            move.make(rc);
        }

        MapLocation[] tanks  = cmd.getTankDeque();
        atk.makeAttack(shouldHitArchon, tanks);
        cmd.addTanktoDeque(rc.getLocation());
        cmd.updateTankTail();
    }

}