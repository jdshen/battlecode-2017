package main.controllers;

import battlecode.common.*;
import main.act.Donate;
import main.act.Shake;
import main.helper.Scouting;
import main.nav.Move;

public strictfp class ScoutController extends Controller {
    private final Shake shake = new Shake();
    private final Donate donate = new Donate();
    private final Scouting scouting = new Scouting();

    public ScoutController(RobotController rc) {
        super(rc);
    }

    @Override
    public void run() throws GameActionException {
        cmd.updateCount();
        m.update();

        donate.donate();

        Controller c = Controller.c;
        RobotController rc = this.rc;
        Team enemy = c.enemy;
        MapLocation here = rc.getLocation();
        // try shakin
        shake.shakeUnsafe();

        // find closest
        RobotInfo[] enemies = rc.senseNearbyRobots(-1, enemy);
        RobotInfo closest = findTarget(enemies);
        MapLocation target;

        if (enemies.length > 0 && enemies[0].type != RobotType.SCOUT) {
            Controller.cmd.sendAttackCommand(enemies[0].location);
        }

        if (closest != null) {
            target = closest.location;
            Move move = Controller.m.follow(closest);
            if (move != null) {
                atk.makeAttackMoveSingle(move, target);
                bugger.endBug();
            }
        } else {
            target = scouting.scout();
            rc.setIndicatorDot(target, 0, 0, 255);
            if (!bugger.hugging) {
                Move move = Controller.m.moveTowards(target);
                if (move != null) {
                    atk.makeAttackMoveSingle(move, target);
                    bugger.endBug();
                }
            }
        }

        if (rc.getMoveCount() == 0) {
            if (!bugger.bugging) {
                bugger.startBug(target);
            }
            Move move = bugger.bug();
            if (move != null) {
                atk.makeAttackMoveSingle(move, target);
            }
        }

        if (rc.getMoveCount() == 0) {
            Move move = Controller.m.dodgeTowards(target);
            if (move != null) {
                atk.makeAttackMoveSingle(move, target);
                bugger.endBug();
            }
        }

        // try shakin again
        shake.shake();
    }

    public RobotInfo findTarget(RobotInfo[] enemies) {
        RobotInfo best = null;
        for (int i = 0; i < Math.min(enemies.length, 16); i++) {
            RobotInfo info = enemies[i];
            if (info.type == RobotType.GARDENER) {
                // dont take body radius into account, or may be unstable from turn to turn
                return info;
            }

            if (best == null && info.type == RobotType.ARCHON) {
                best = info;
            }
        }

        return best;
    }
}