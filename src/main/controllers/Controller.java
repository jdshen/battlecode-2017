package main.controllers;

import battlecode.common.GameActionException;
import battlecode.common.RobotController;
import battlecode.common.RobotType;
import battlecode.common.Team;
import main.message.CommandManager;
import main.micro.Attack;
import main.micro.AttackType;
import main.nav.Bugger;
import main.nav.Mover;

public abstract class Controller {
    // shortcuts for basic information
    public final int id;
    public final Team team;
    public final Team enemy;
    public final RobotController rc;
    public final RobotType type;
    public long[] teamMemory;

    // shortcuts for type
    public final float attackPower;
    public final float bodyRadius;
    public final float sr;
    public final float bsr;
    public final float bs;
    public final float stride;
    public final AttackType maxAttackType;

    public final int maxHealth;
    public final int cost;
    public final int buildCooldownTurns;

    public final int bytecodeLimit;

    public Controller(RobotController rc) {
        team = rc.getTeam();
        enemy = team.opponent();
        id = rc.getID();
        type = rc.getType();
        teamMemory = rc.getTeamMemory();
        this.rc = rc;

        attackPower = type.attackPower;
        bodyRadius = type.bodyRadius;
        bsr = type.bulletSightRadius;
        bs = type.bulletSpeed;
        sr = type.sensorRadius;
        stride = type.strideRadius;
        switch (type) {
        case SCOUT:
            maxAttackType = AttackType.SINGLE;
            break;
        case SOLDIER:
            maxAttackType = AttackType.PENTAD;
            break;
        case TANK:
            maxAttackType = AttackType.PENTAD;
            break;
        default:
            maxAttackType = AttackType.NONE;
            break;
        }

        maxHealth = type.maxHealth;
        buildCooldownTurns = type.buildCooldownTurns;
        cost = type.bulletCost;

        bytecodeLimit = type.bytecodeLimit;

        setController(this);
    }

    public abstract void run() throws GameActionException;

    // Defaults for all classes
    public static Controller c;
    public static RobotController crc;
    public static Mover m;
    public static CommandManager cmd;
    public static Bugger bugger;
    public static Attack atk;


    public static void setController(Controller c) {
        Controller.c = c;
        Controller.crc = c.rc;
        cmd = new CommandManager();
        if (c.type != RobotType.LUMBERJACK) {
            m = new Mover(true, true);
        } else {
            m = new Mover(true, false);
        }

        if (c.type == RobotType.SCOUT) {
            bugger = new Bugger(m);
        } else {
            bugger = new Bugger();
        }
        atk = new Attack();
    }
}
