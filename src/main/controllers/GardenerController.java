package main.controllers;

import battlecode.common.*;
import main.act.Donate;
import main.act.Shake;
import main.act.Water;
import main.helper.BuildOrder;
import main.helper.BuildType;
import main.message.Command;
import main.message.CommandManager;
import main.message.CommandType;
import main.nav.Bugger;
import main.nav.Move;
import main.util.Math2;
import main.util.OtherGameConstants;

public strictfp class GardenerController extends Controller {
    private MapLocation target;
    private boolean oasis = false; // true if found a good spot to build
    private MapLocation open;
    private float openness = bodyRadius;
    int oasisLimit; // arbitrary constant

    private Shake shake = new Shake();
    private Water water = new Water();
    private Donate donate = new Donate();
    private BuildOrder buildOrder = new BuildOrder();

    public GardenerController(RobotController rc) {
        super(rc);
        target = rc.getInitialArchonLocations(enemy)[0];
        bugger.startBug(target);
        open = rc.getLocation();
        oasisLimit = (int) rc.getLocation().distanceTo(target) * 2 / 3; // stride = 0.5
    }


    public void plant() throws GameActionException {
        RobotController rc = this.rc;
        MapLocation here = rc.getLocation();

        Direction dir = Math2.directionTo(target, here);
        float DEGREES = 60;
        for (int i = (int)(OtherGameConstants.MAX_DEGREES / DEGREES); --i >= 0; ) {
            if (rc.canPlantTree(dir)) {
                cmd.build(BuildType.TREE);
                rc.plantTree(dir);
                return;
            }

            dir = dir.rotateRightDegrees(DEGREES);
        }
    }

    public void build(RobotType unit) throws GameActionException {
        RobotController rc = this.rc;
        MapLocation here = rc.getLocation();

        Direction dir = Math2.directionTo(target, here);
        float DEGREES = 30;

        for (int i = (int)(OtherGameConstants.MAX_DEGREES / DEGREES); --i >= 0; ) {
            if (rc.canBuildRobot(unit, dir)) {
                cmd.build(BuildType.getBuildType(unit));
                rc.buildRobot(unit, dir);
                return;
            }
            dir = dir.rotateLeftDegrees(DEGREES);
        }
    }

    public void move() throws GameActionException {
        RobotController rc = this.rc;
        MapLocation here = rc.getLocation();

        if (bugger.bugging) {
            if (bugger.to.isWithinDistance(here, OtherGameConstants.EPSILON)) {
                bugger.endBug();
                return;
            }

            if (rc.canSenseLocation(bugger.to) && rc.isLocationOccupiedByTree(bugger.to)) {
                bugger.endBug();
                return;
            }

            Move move = bugger.bug();
            if (move != null) {
                move.make(rc);
                return;
            }
        }
    }


    public boolean calcOasis() throws GameActionException {
        RobotController rc = this.rc;
        MapLocation here = rc.getLocation();

        float DEGREES = 60;
        float PLANT_RADIUS = OtherGameConstants.PLANT_RADIUS;
        float DIST = sr - PLANT_RADIUS - OtherGameConstants.EPSILON;
        Direction dir = Math2.directionTo(here, target);
        for (int i = (int)(OtherGameConstants.MAX_DEGREES / DEGREES); --i >= 0; ) {
            MapLocation loc = here.add(dir, DIST);
            if (!rc.isCircleOccupiedExceptByThisRobot(loc, PLANT_RADIUS) &&
                    rc.onTheMap(loc, PLANT_RADIUS)) {
                bugger.startBug(loc);
                return true;
            }

            dir = dir.rotateLeftDegrees(DEGREES);
        }

        DIST--;
        for (int i = (int)(OtherGameConstants.MAX_DEGREES / DEGREES); --i >= 0; ) {
            MapLocation loc = here.add(dir, DIST);
            if (!rc.isCircleOccupiedExceptByThisRobot(loc, PLANT_RADIUS) &&
                    rc.onTheMap(loc, PLANT_RADIUS)) {
                bugger.startBug(loc);
                return true;
            }

            dir = dir.rotateLeftDegrees(DEGREES);
        }

        boolean unoccupied = !rc.isCircleOccupiedExceptByThisRobot(here, openness + 0.5f) &&
                rc.onTheMap(here, openness + 0.5f);
        if (unoccupied) {
            openness += 0.5f;
            open = here;
        }

        if (oasisLimit == 0 || openness >= PLANT_RADIUS) {
            bugger.startBug(open);
            return true;
        }

        oasisLimit--;

        return false;
    }

    @Override
    public void run() throws GameActionException {
        donate.donate();
        cmd.updateCount();
        m.update();

        RobotController rc = this.rc;
        MapLocation here = rc.getLocation();

        int[] unitCounts = cmd.getUnitCounts();
        int[] buildCounts = cmd.getBuiltCounts();
        int trees = rc.getTreeCount();

        shake.shake();
        water.water();

        if (!oasis) {
            oasis = calcOasis();
        }

        TreeInfo[] neutralTrees = rc.senseNearbyTrees(-1, Team.NEUTRAL);
        if (!bugger.bugging && neutralTrees.length > 0) {
            Controller.cmd.bulkSetCommand(new Command(here, CommandType.CLEAR, rc.getRoundNum()), RobotType.LUMBERJACK);
        }

        if (unitCounts != null) {
            BuildType type = buildOrder.next(buildCounts, unitCounts, trees);
            if (type != null) {
                if (rc.isBuildReady() && type.type != null) {
                    build(type.type);
                } else if (type.tree && !bugger.bugging) {
                    plant();
                } else if (rc.isBuildReady()) {
                    // cant reach
                    build(RobotType.LUMBERJACK);
                }
            }
        }

        move();
    }

}