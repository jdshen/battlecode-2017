package main.controllers;

import battlecode.common.*;
import main.act.Donate;
import main.act.Shake;
import main.message.Command;
import main.message.CommandType;
import main.nav.Move;
import main.nav.Mover;
import main.util.Math2;
import main.util.OtherGameConstants;

public strictfp class LumberjackController extends Controller {
    private final Shake shake = new Shake();
    private final Donate donate = new Donate();

    private MapLocation defaultTarget = null;
    private CommandType currentCommand = null;

    public LumberjackController(RobotController rc) {
        super(rc);
    }

    @Override
    public void run() throws GameActionException {
        cmd.updateCount();
        m.update();
        donate.donate();

        Controller c = Controller.c;
        RobotController rc = this.rc;
        Team enemy = c.enemy;
        // initialize the goal
        if (defaultTarget == null) {
            MapLocation[] archonLocs = rc.getInitialArchonLocations(enemy);
            defaultTarget = archonLocs[c.id % archonLocs.length];
        }

        Command cmd = Controller.cmd.readCommand();
        if (cmd != null && cmd.isFresh()) {
            currentCommand = cmd.command;
            defaultTarget = cmd.location;
        } else {
            MapLocation[] archonLocs = rc.getInitialArchonLocations(enemy);
            currentCommand = CommandType.LUMBER;
            defaultTarget = archonLocs[c.id % archonLocs.length];
        }

        // try shakin
        shake.shakeUnsafe();

        MapLocation target = findTarget();
        Move move = Controller.m.moveTowards(target);
        if (move != null && !bugger.hugging) {
            bugger.endBug();
            move.make(rc);
        }

        // check atk (if haven't chop-moved)
        boolean attacked = false;
        if (rc.canStrike()) {
            attacked = attack(target);
        }

        if (attacked) {
            bugger.endBug();
        } else if (!bugger.bugging && move == null){
            bugger.startBug(target);
        }

        if (bugger.bugging) {
            Move bug = bugger.bug();
            if (bug != null) {
                bug.make(rc);
            }
        }

        shake.shake();
    }

    public MapLocation findTarget() throws GameActionException {
        MapLocation target = null;
        RobotController rc = Controller.crc;
        float distTo = OtherGameConstants.MAX_DIST;
        MapLocation here = rc.getLocation();

        TreeInfo[] enemyTrees = rc.senseNearbyTrees(-1, enemy);
        if (enemyTrees.length > 0) {
            target = enemyTrees[0].location;
            distTo = here.distanceTo(target);
        }

        RobotInfo[] enemies = rc.senseNearbyRobots(-1, enemy);
        if (enemies.length > 0) {
            MapLocation loc = enemies[0].location;
            float dist = here.distanceTo(loc);
            if (dist < distTo) {
                target = loc;
                return target;
            }
        }

        if (target != null) {
            return target;
        }

        // (if clear, move towards closest neutral tree thing in location
        TreeInfo[] neutralTrees = rc.senseNearbyTrees(-1, Team.NEUTRAL);
        if (neutralTrees.length > 0) {
            for (int i = 0; i < Math.min(neutralTrees.length, 16); i++) {
                if (neutralTrees[i].containedRobot != null) {
                    return neutralTrees[i].location;
                }
            }
        }

        // if clearing, try to move towards trees you see near the goal location (if nothing else)
        if (currentCommand == CommandType.CLEAR) {
            TreeInfo[] treesToClear = rc.senseNearbyTrees(defaultTarget, -1, Team.NEUTRAL);
            if (treesToClear.length > 0) {
                TreeInfo tree = null;
                float best = OtherGameConstants.MAX_DIST;
                for (int i = Math.min(treesToClear.length, 16); --i >= 0; ) {
                    TreeInfo info = treesToClear[i];
                    float dist = info.location.distanceTo(defaultTarget);
                    if (dist < best) {
                        best = dist;
                        tree = info;
                    }
                }
                return tree.location;
            }
        }

        return defaultTarget;
    }

    // returns true if attacked in direction
    public boolean attack(MapLocation target) throws GameActionException {
        RobotController rc = Controller.crc;
        Controller c = Controller.c;
        MapLocation here = rc.getLocation();
        Team enemy = c.enemy;
        Team ally = c.team;
        int strikeScore = 0;
        float strikeDistance = GameConstants.LUMBERJACK_STRIKE_RADIUS;

        RobotInfo[] robots = rc.senseNearbyRobots(strikeDistance);
        for (int i = robots.length; --i >= 0;) {
            RobotInfo robot = robots[i];
            if (robot.team == enemy) {
                switch (robot.type) {
                case ARCHON:
                case GARDENER:
                    // archons and gardeners are worth striking always
                    strikeScore += 40;
                    break;
                default:
                    // units prob worth a chop
                    strikeScore += 5;
                    break;
                }
            } else {
                // for allies
                switch (robot.type) {
                case ARCHON:
                    // don't fuck ourselves
                    strikeScore -= 10000;
                case GARDENER:
                    strikeScore -= 40;
                    break;
                case LUMBERJACK:
                    // hitting own lumberjacks is w.e
                    strikeScore -= 3;
                    break;
                default:
                    // don't hit own units
                    strikeScore -= 6;
                    break;
                }
            }
        }

        TreeInfo[] enemyTrees = rc.senseNearbyTrees(strikeDistance, enemy);
        strikeScore += enemyTrees.length;
        TreeInfo[] allyTrees = rc.senseNearbyTrees(strikeDistance, ally);
        strikeScore -= allyTrees.length;

        if (strikeScore > GameConstants.LUMBERJACK_CHOP_DAMAGE) {
            rc.strike();
            return true;
        }

        // try to chop an enemy tree
        // ASSUMING THAT STRIKE AREA (BODY + STRIKE-R) > CHOP AREA (INTERACTION DIST + BODY RADIUS)
        for (int i = enemyTrees.length; --i >= 0;) {
            TreeInfo tree = enemyTrees[i];
            if (rc.canChop(tree.ID)) {
                rc.chop(tree.ID);
                return true;
            }
        }

        // chop tree with closest radians in direction
        float chopDist = c.bodyRadius + GameConstants.INTERACTION_DIST_FROM_EDGE;
        TreeInfo[] neutralTrees = rc.senseNearbyTrees(chopDist,Team.NEUTRAL);
        Direction dir = Math2.directionTo(here, target);
        int id = -1;
        float bestAngle = OtherGameConstants.MAX_DEGREES;
        for (int i = Math.min(neutralTrees.length, 16); --i >= 0;) {
            TreeInfo tree = neutralTrees[i];
            if (rc.canChop(tree.getID())) {
                float angle = StrictMath.abs(dir.radiansBetween(here.directionTo(tree.location)));
                if (angle < bestAngle) {
                    bestAngle = angle;
                    id = tree.getID();
                }
            }
        }

        if (id >= 0 && bestAngle <= Mover.DEGREES_TOWARDS) {
            rc.chop(id);
            return true;
        }

        if (strikeScore > 0) {
            rc.strike();
            return false;
        }

        if (id >= 0) {
            rc.chop(id);
            return false;
        }

        return false;
    }
}