package main;
import battlecode.common.*;
import main.controllers.*;

public strictfp class RobotPlayer {
    /**
     * run() is the method that is called when a robot is instantiated in the Battlecode world.
     * If this method returns, the robot dies!
     **/
    @SuppressWarnings("unused")
    public static void run(RobotController rc) {
        Controller cont = null;
        do {
            try {
                switch (rc.getType()) {
                    case ARCHON:
                        cont = new ArchonController(rc);
                        break;
                    case GARDENER:
                        cont = new GardenerController(rc);
                        break;
                    case LUMBERJACK:
                        cont = new LumberjackController(rc);
                        break;
                    case SCOUT:
                        cont = new ScoutController(rc);
                        break;
                    case SOLDIER:
                        cont = new SoldierController(rc);
                        break;
                    case TANK:
                        cont = new TankController(rc);
                        break;
                    default:
                        cont = null;
                        break;
                }
            } catch (Exception e) {
            }
        } while (cont == null);

        while (true) {
            try {
                cont.run();
            } catch (Exception e) {
                e.printStackTrace(System.out);
            }
            Clock.yield();
        }
    }
}