package main.util;

import battlecode.common.MapLocation;

public class FastIdLocationMap {
    private StringBuilder has = new StringBuilder();
    private int HASH = 1009;
    // should never be more than 1000 trees
    private MapLocation[] ids = new MapLocation[HASH];

    // Not: only call if not present
    public void add(int id, MapLocation loc) {
        int idd = id % HASH;
        has.append((char) idd);
        ids[idd] = loc;
    }

    public void remove(int id) {
        int idd = id % HASH;
        String s = new StringBuilder()
            .append((char) id)
            .toString();
        int index = has.indexOf(s);
        if (index != -1) {
            has.deleteCharAt(index);
        }
        ids[idd] = null;
    }

    public boolean contains(int id) {
        return ids[id % HASH] != null;
    }

    public MapLocation get(int id) {
        return ids[id % HASH];
    }

    public int size() {
        return has.length();
    }

    public MapLocation pop() {
        if (has.length() > 0) {
            int id = has.charAt(0); // no modding
            has.deleteCharAt(0);
            MapLocation loc = ids[id];
            ids[id] = null;
            return loc;
        }
        return null;
    }
}
