package main.util;

public class FastIdSet {
    public StringBuilder has;

    public FastIdSet() {
        has = new StringBuilder();
    }

    public void add(int id) {
        has.append(';')
            .append(id);
    }

    public boolean contains(int id) {
        String s = new StringBuilder()
            .append(';')
            .append(id)
            .toString();
        return has.indexOf(s) != -1;
    }

    public void clear() {
        has = new StringBuilder();
    }
}
