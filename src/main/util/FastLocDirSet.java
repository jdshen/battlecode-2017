package main.util;
import battlecode.common.*;

public strictfp class FastLocDirSet {
    public StringBuilder has;
    private final int gran; // granularity of degree measure

    public FastLocDirSet() {
        this(45);
    }

    public FastLocDirSet(int gran) {
        has = new StringBuilder();
        this.gran = gran;
    }

    public void add(MapLocation loc, Direction dir) {
        has.append(';')
            .append((int) (loc.x * 2))
            .append(',')
            .append((int) (loc.y * 2))
            .append(',')
            .append((int)(dir.getAngleDegrees() / gran));
    }

    public boolean contains(MapLocation loc, Direction dir) {
        String s = new StringBuilder()
            .append(';')
            .append((int) (loc.x * 2))
            .append(',')
            .append((int) (loc.y * 2))
            .append(',')
            .append((int)(dir.getAngleDegrees() / gran))
            .toString();
        return has.indexOf(s) != -1;
    }

    public void clear() {
        has = new StringBuilder();
    }
}