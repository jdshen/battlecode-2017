package main.util;

import battlecode.common.MapLocation;
import battlecode.common.RobotType;
import main.controllers.Controller;

/**
 * Stores locs -> round, with a given granularity. Does not clear.
 */
public class FastLocRoundSet {
    // 400 / 14
    private int[][] round;
    private final int block;
    private final static int HASH = OtherGameConstants.MAX_MAP_SIDE * 2;

    public FastLocRoundSet(int block) {
        this.block = block;
        round = new int[HASH / block + 1][HASH / block + 1];
    }

    public int get(MapLocation loc) {
        return round[((int) loc.x % HASH) / block][((int) loc.y % HASH) / block];
    }

    public void add(MapLocation loc, int roundNum) {
        round[((int) loc.x % HASH) / block][((int) loc.y % HASH) / block] = roundNum;
    }
}
