package main.util;

import battlecode.common.GameConstants;
import battlecode.common.RobotType;
import main.helper.BuildType;
import main.message.CommandType;

import static battlecode.common.GameConstants.BULLET_TREE_MAX_HEALTH;

public strictfp class OtherGameConstants {
    public static final int MAX_MAP_OFFSET = 500;
    public static final int MAX_NEGATIVE_MAP_OFFSET = 0;
    public static final int MAX_ROUND = 3001;
    public static final int DEGREE_OFFSET = 180; // number of degrees needed to offset degree to 0
    public static final float MAX_DEGREES = 360;
    public static final float MAX_DIST =
        (float) StrictMath.sqrt(GameConstants.MAP_MAX_HEIGHT * GameConstants.MAP_MAX_HEIGHT +
            GameConstants.MAP_MAX_WIDTH * GameConstants.MAP_MAX_WIDTH);
    public static final int MAX_MAP_SIDE = Math2.max(GameConstants.MAP_MAX_HEIGHT, GameConstants.MAP_MAX_WIDTH);
    public static final int NUM_ROBOT_TYPES = RobotType.values().length;
    public static final int NUM_BUILD_TYPES = BuildType.values().length;

    public static final float PLANT_RADIUS =
        GameConstants.BULLET_TREE_RADIUS * 2 + RobotType.GARDENER.bodyRadius * 1.5f;
    public static final float DELTA = 0.05f;
    public static final float EPSILON = 0.001f;
    public static final float MILLI_EPSILON = 0.000001f;
    public static final float MAX_COST = 300;

    public static final float STD_SIGHT_RADIUS = RobotType.SCOUT.sensorRadius;

}
