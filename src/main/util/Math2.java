package main.util;

import battlecode.common.BodyInfo;
import battlecode.common.Direction;
import battlecode.common.MapLocation;

/**
 * Created by jdshen on 1/5/16.
 */
public strictfp class Math2 {
    public static int max(int x, int y) {
        return x > y ? x : y;
    }

    public static int min(int x, int y) {
        return x < y ? x : y;
    }

    public static int floorDivision(int n, int d) {
        return n >= 0 ? n / d : ~(~n / d);
    }

    public static Direction directionTo(MapLocation a, MapLocation b) {
        Direction dir = a.directionTo(b);
        if (dir != null) {
            return dir;
        }

        return Direction.getNorth();
    }

    public static Direction avg(Direction a, Direction b) {
        return a.rotateLeftDegrees(a.degreesBetween(b) / 2);
    }

    public static Direction dirAvg(MapLocation here, MapLocation a, MapLocation b) {
        return avg(here.directionTo(a), here.directionTo(b));
    }

    public static Direction dirAvg(MapLocation here, BodyInfo a, BodyInfo b) {
        return dirAvg(here, a.getLocation(), b.getLocation());
    }
}
