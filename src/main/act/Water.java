package main.act;

import battlecode.common.*;
import main.controllers.Controller;

public strictfp class Water {
    /**
     *  should be passed an array of trees within water range.
     *  checks that the tree is on this team
     * @param waterableTrees
     * @throws GameActionException
     */
    public void waterUnsafe(TreeInfo[] waterableTrees) throws GameActionException {
        RobotController rc = Controller.crc;
        Team myTeam = Controller.c.team;
        int bestTreeId = -1;
        float bestTreeHeal = 0;
        for (int i = waterableTrees.length; --i >= 0;) {
            TreeInfo treeInfo = waterableTrees[i];
            if (treeInfo.team == myTeam) {
                float heal = treeInfo.maxHealth - treeInfo.health;
                if (heal > bestTreeHeal) {
                    bestTreeId = treeInfo.ID;
                    bestTreeHeal = heal;
                }
            }
        }
        if (bestTreeId >= 0) {
            rc.water(bestTreeId);
        }
    }

    /**
     *  should be passed an array of trees within water range;
     *  checks if robot has watered before, and that the tree is on this team
     * @param waterableTrees
     * @throws GameActionException
     */
    public void water(TreeInfo[] waterableTrees) throws GameActionException {
        RobotController rc = Controller.crc;
        if (rc.canWater()) {
            waterUnsafe(waterableTrees);
        }
    }

    /**
     * shortcut to water a nearby tree
     * @throws GameActionException
     */
    public void water() throws GameActionException {
        RobotController rc = Controller.crc;
        Controller c = Controller.c;
        if (rc.canWater()) {
            waterUnsafe(rc.senseNearbyTrees(GameConstants.INTERACTION_DIST_FROM_EDGE + c.bodyRadius, c.team));
        }
    }
}
