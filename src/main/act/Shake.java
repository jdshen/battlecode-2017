package main.act;

import battlecode.common.*;
import main.controllers.Controller;

public strictfp class Shake {
    /**
     * should be passed an array of trees within shake range. does no safety checks.
     * @param shakeableTrees
     * @throws GameActionException
     */
    public void shakeUnsafe(TreeInfo[] shakeableTrees) throws GameActionException {
        RobotController rc = Controller.crc;
        for (int i = shakeableTrees.length; --i >= 0;) {
            TreeInfo treeInfo = shakeableTrees[i];
            if (treeInfo.getContainedBullets() > 0) {
                rc.shake(treeInfo.ID);
                break;
            }
        }
    }

    /**
     * should be passed an array of trees within shake range. check if has shaken before
     * @param shakeableTrees
     * @throws GameActionException
     */
    public void shake(TreeInfo[] shakeableTrees) throws GameActionException {
        RobotController rc = Controller.crc;
        if (rc.canShake()) {
            shakeUnsafe(shakeableTrees);
        }
    }

    /**
     * try to shake a valid nearby tree. does not check if has shaken before.
     * @throws GameActionException
     */
    public void shakeUnsafe() throws GameActionException {
        Controller c = Controller.c;
        RobotController rc = Controller.crc;
        shakeUnsafe(rc.senseNearbyTrees(GameConstants.INTERACTION_DIST_FROM_EDGE + c.bodyRadius, Team.NEUTRAL));
    }

    /**
     * try to shake a valid nearby tree. checks if has shaken before
     * @throws GameActionException
     */
    public void shake() throws GameActionException {
        RobotController rc = Controller.crc;
        Controller c = Controller.c;
        if (rc.canShake()) {
            shakeUnsafe(rc.senseNearbyTrees(GameConstants.INTERACTION_DIST_FROM_EDGE + c.bodyRadius, Team.NEUTRAL));
        }
    }
}
