package main.act;

import battlecode.common.*;
import main.controllers.Controller;
import main.helper.BuildOrder;
import main.helper.BuildType;
import main.util.OtherGameConstants;

// Archon class that spawns gardeners
public strictfp class Hire {
    private MapLocation target;
    private static final float DEGREES = 15;
    private BuildOrder buildOrder = new BuildOrder();

    public Hire() {
        target = Controller.crc.getInitialArchonLocations(Controller.c.enemy)[0];
    }

    private void tryHire() throws GameActionException {
        RobotController rc = Controller.crc;
        MapLocation here = rc.getLocation();
        Direction dir = target.directionTo(here);
        float DEGREES = Hire.DEGREES;
        for (int i = (int)(OtherGameConstants.MAX_DEGREES / DEGREES + 0.5) - 1; i >= 0; i--) {
            if (rc.canHireGardener(dir)) {
                rc.hireGardener(dir);
                Controller.cmd.build(BuildType.GARDENER);
                return;
            }
            dir = dir.rotateLeftDegrees(DEGREES);
        }

    }

    public void hire(int[] buildCounts, int[] unitCounts, int trees) throws GameActionException {
        RobotController rc = Controller.crc;
        if (!(rc.isBuildReady() && rc.getTeamBullets() >= RobotType.GARDENER.bulletCost)) {
            return;
        }

        BuildType type = buildOrder.next(buildCounts, unitCounts, trees);
        if (type != null) {
            tryHire();
        }
    }
}
