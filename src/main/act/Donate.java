package main.act;

import battlecode.common.GameActionException;
import battlecode.common.GameConstants;
import battlecode.common.RobotController;
import main.controllers.Controller;
import main.util.OtherGameConstants;

import java.awt.*;

/**
 * helper functions for donating, usually around not overdonating
 */
public strictfp class Donate {
    /**
     * Default donate command.
     */
    public void donate() throws GameActionException {
        maybeWin();
        if (Controller.crc.getTeamBullets() > OtherGameConstants.MAX_COST * 2) {
            donateDownTo(OtherGameConstants.MAX_COST * 2);
        }
    }

    /**
     * Defaults down to a certain amount
     */
    public void donateDownTo(float bullets) throws GameActionException {
        RobotController rc = Controller.crc;
        float rate = rc.getVictoryPointCost();
        float b = Controller.crc.getTeamBullets() - bullets;
        Controller.crc.donate((int)(b / rate) * rate + OtherGameConstants.MILLI_EPSILON);
    }

    // if we can win, do it.
    public void maybeWin() throws GameActionException {
        RobotController rc = Controller.crc;
        float teamBullets = rc.getTeamBullets();
        float rate = rc.getVictoryPointCost();

        if ((int)(teamBullets / rate) + rc.getTeamVictoryPoints() >= GameConstants.VICTORY_POINTS_TO_WIN) {
            rc.donate(rc.getTeamBullets());
        } else if (rc.getRoundNum() >= rc.getRoundLimit() - 1) {
            rc.donate(rc.getTeamBullets());
        }
    }
}
