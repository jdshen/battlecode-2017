package main.micro;

public strictfp enum AttackType {
    SINGLE,
    TRIAD,
    PENTAD,
    NONE,
}
