package main.micro;

import battlecode.common.MapLocation;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;
import battlecode.common.Team;
import main.controllers.Controller;

public class Avoid {
    private final static int MAX_UNITS = 8;

    private RobotInfo[] infos;
    private int num;
    private float MAX_SENSE = Controller.c.stride + Controller.c.bodyRadius
            + RobotType.TANK.bulletSpeed + RobotType.TANK.strideRadius;


    public void update() {
        RobotInfo[] sense = Controller.crc.senseNearbyRobots(MAX_SENSE, Controller.c.enemy);
        RobotInfo[] infos = new RobotInfo[MAX_UNITS];
        int num = 0;

        int length = Math.min(sense.length, 16);
        for (int i = 0; i < length; i++) {
            if (sense[i].type.canAttack()) {
                infos[num++] = sense[i];
                if (num >= MAX_UNITS) {
                    break;
                }
            }
        }

        this.infos = infos;
        this.num = num;
    }

    public float damageAtLoc(MapLocation loc) {
        RobotInfo[] infos = this.infos;
        RobotType type = Controller.c.type;

        float dmg = 0;
        for (int i = num; --i >= 0; ) {
            RobotInfo info = infos[i];
            if (info.location.distanceTo(loc) >= avoidDistance(type, info.type)) {
                continue;
            }

            dmg += info.type.attackPower;
        }

        return dmg;
    }

    private float avoidDistance(RobotType me, RobotType them) {
        switch (me) {
            case ARCHON:
                switch(them) {
                    case ARCHON:
                        return 0.0f;
                    case GARDENER:
                        return 0.0f;
                    case LUMBERJACK:
                        return 5.75f;
                    case SOLDIER:
                        return 5.45f;
                    case TANK:
                        return 8.0f;
                    case SCOUT:
                        return 5.25f;
                    default:
                        return 0;
                }
            case GARDENER:
                switch(them) {
                    case ARCHON:
                        return 0.0f;
                    case GARDENER:
                        return 0.0f;
                    case LUMBERJACK:
                        return 4.75f;
                    case SOLDIER:
                        return 4.45f;
                    case TANK:
                        return 7.0f;
                    case SCOUT:
                        return 4.25f;
                    default:
                        return 0;
                }
            case LUMBERJACK:
                switch(them) {
                    case ARCHON:
                        return 0.0f;
                    case GARDENER:
                        return 0.0f;
                    case LUMBERJACK:
                        return 4.75f;
                    case SOLDIER:
                        return 4.2f;
                    case TANK:
                        return 6.75f;
                    case SCOUT:
                        return 4.0f;
                    default:
                        return 0;
                }
            case SOLDIER:
                switch(them) {
                    case ARCHON:
                        return 0.0f;
                    case GARDENER:
                        return 0.0f;
                    case LUMBERJACK:
                        return 4.75f;
                    case SOLDIER:
                        return 4.0f;
                    case TANK:
                        return 6.55f;
                    case SCOUT:
                        return 3.8f;
                    default:
                        return 0;
                }
            case TANK:
                switch(them) {
                    case ARCHON:
                        return 0.0f;
                    case GARDENER:
                        return 0.0f;
                    case LUMBERJACK:
                        return 5.75f;
                    case SOLDIER:
                        return 5.45f;
                    case TANK:
                        return 8.0f;
                    case SCOUT:
                        return 5.25f;
                    default:
                        return 0;
                }
            case SCOUT:
                switch(them) {
                    case ARCHON:
                        return 0.0f;
                    case GARDENER:
                        return 0.0f;
                    case LUMBERJACK:
                        return 4.75f;
                    case SOLDIER:
                        return 7.362f;
                    case TANK:
                        return 6.912f;
                    case SCOUT:
                        return 7.662f;
                    default:
                        return 0;
                }
        }
        return 0;
    }
}
