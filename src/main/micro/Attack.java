package main.micro;

import battlecode.common.*;
import main.controllers.Controller;
import main.nav.Move;
import main.util.FastIdSet;
import main.util.Math2;
import main.util.OtherGameConstants;

public class Attack {
    private static int GRANULARITY = 12;
    private static float DEGREES = 30;

    public static final float BULLET_CHECK_INTERVAL = .35f;
    public static final float BULLET_CHECK_INTERVAL_CLOSE = .25f;
    // for if we use out of sight targeting
    public static final float MAX_DIST = 6;

    private FastIdSet bIds = new FastIdSet();

    public void makeAttack(boolean shouldHitArchon, MapLocation[] tanks) throws GameActionException {
        AttackInfo attackInfo = attack(shouldHitArchon, tanks);
        RobotController rc = Controller.crc;
        if (attackInfo != null) {
            switch(attackInfo.type) {
                case PENTAD:
                    if (rc.canFirePentadShot()) {
                        rc.firePentadShot(attackInfo.dir);
                        break;
                    }
                case TRIAD:
                    if (rc.canFireTriadShot()) {
                        rc.fireTriadShot(attackInfo.dir);
                    }
                    break;
                case SINGLE:
                    if (rc.canFireSingleShot()) {
                        rc.fireSingleShot(attackInfo.dir);
                    }
                    break;
                default:
                    break;
            }
        }
    }

    public void makeAttack(boolean shouldHitArchon) throws GameActionException {
        makeAttack(shouldHitArchon, null);
    }

    public void makeAttackMove(Move m, MapLocation target, boolean shouldHitArchon) throws GameActionException {
        if (target == null || StrictMath.abs(Math2.directionTo(Controller.crc.getLocation(), target).degreesBetween(m.dir)) < 90) {
            m.make(Controller.crc);
            makeAttack(shouldHitArchon);
        } else {
            makeAttack(shouldHitArchon);
            m.make(Controller.crc);
        }
    }

    public void makeAttackMoveSingle(Move m, MapLocation target) throws GameActionException {
        if (target == null || StrictMath.abs(Math2.directionTo(Controller.crc.getLocation(), target).degreesBetween(m.dir)) < 90) {
            m.make(Controller.crc);
            makeAttackSingle();
        } else {
            makeAttackSingle();
            m.make(Controller.crc);
        }
    }

    public void makeAttackSingle() throws GameActionException {
        AttackInfo attackInfo = attackSingle();
        RobotController rc = Controller.crc;
        if (attackInfo != null) {
            switch(attackInfo.type) {
                case PENTAD:
                    if (rc.canFirePentadShot()) {
                        rc.firePentadShot(attackInfo.dir);
                        break;
                    }
                case TRIAD:
                    if (rc.canFireTriadShot()) {
                        rc.fireTriadShot(attackInfo.dir);
                    }
                    break;
                case SINGLE:
                    if (rc.canFireSingleShot()) {
                        rc.fireSingleShot(attackInfo.dir);
                    }
                    break;
                default:
                    break;
            }
        }
    }

    public AttackInfo attackOutOfRange(MapLocation[] tanks) throws GameActionException {
        RobotController rc = Controller.crc;
        RobotInfo[] enemies = rc.senseNearbyRobots(-1, Controller.c.enemy);
        MapLocation here = rc.getLocation();
        float sr = Controller.c.sr;
        float radius = Controller.c.bodyRadius;
        if (enemies.length > 0) {
            return null;
        }

        float whitelist = (float) StrictMath.sqrt(2) + radius + GameConstants.BULLET_SPAWN_OFFSET + OtherGameConstants.EPSILON;
        BulletInfo[] bullets = rc.senseNearbyBullets();
        for (int i = bullets.length; --i >= 0; ) {
            BulletInfo bullet = bullets[i];
            float distance = bullet.location.distanceTo(here);

            if (distance < sr) {
                break;
            }

            if (bIds.contains(bullet.ID)) {
                continue;
            }

            bIds.add(bullet.ID);

            Direction dir = bullet.location.directionTo(here);

            // calculate angle range where you get hit
            double maxAngle = StrictMath.asin(radius/distance);
            float radsBetween = dir.radiansBetween(bullet.dir);
            if (StrictMath.abs(radsBetween) <= maxAngle) {
                boolean fire = true;
                for (int j = tanks.length; --j >= 0; ) {
                    if (bullet.location.isWithinDistance(tanks[j], whitelist)) {
                        fire = false;
                        break;
                    }
                }

                if (fire) {
                    if (rc.canFirePentadShot()) {
                        return new AttackInfo(AttackType.PENTAD, dir.opposite());
                    }

                    if (rc.canFireTriadShot()) {
                        return new AttackInfo(AttackType.TRIAD, dir.opposite());
                    }

                    return new AttackInfo(AttackType.SINGLE, dir.opposite());
                }
            }
        }

        return null;
    }

    // for tanks and soldiers
    public AttackInfo attack(boolean shouldHitArchon, MapLocation[] tanks) throws GameActionException {
        RobotController rc = Controller.crc;
        if (!rc.canFireSingleShot()) {
            return null;
        }

        if (tanks != null) {
            AttackInfo info = attackOutOfRange(tanks);
            if (info != null) {
                return info;
            }
        }

        TreeInfo[] trees = rc.senseNearbyTrees();
        RobotInfo[] units = rc.senseNearbyRobots();

        int[] score = new int[GRANULARITY];
        Team team = Controller.c.team;
        Team enemy = Controller.c.enemy;
        MapLocation here = rc.getLocation();

        boolean hasAllies = false;

        // calculate closest units
        int unitsLength = Math.min(units.length, 16);
        RobotInfo[] closestUnits = new RobotInfo[GRANULARITY];
        for (int i = 0; i < unitsLength; i++) {
            Direction dir = here.directionTo(units[i].location);
            int d = Math.min(
                (int) ((dir.getAngleDegrees() + OtherGameConstants.DEGREE_OFFSET) / DEGREES),
                GRANULARITY - 1
            );

            if (closestUnits[d] == null) {
                closestUnits[d] = units[i];
            }

            score[d] += units[i].team == team ? -1 : 1;
            if (units[i].team == team && units[i].type == RobotType.SOLDIER) {
                hasAllies = true;
            }
        }

        // calculate closest trees
        TreeInfo[] closestTrees = new TreeInfo[GRANULARITY];
        int treesLength = Math.min(trees.length, 16);
        for (int i = 0; i < treesLength; i++) {
            Direction dir = here.directionTo(trees[i].location);
            int d = Math.min(
                (int) ((dir.getAngleDegrees() + OtherGameConstants.DEGREE_OFFSET) / DEGREES),
                GRANULARITY - 1
            );

            if (closestTrees[d] == null) {
                closestTrees[d] = trees[i];
            }

            score[d] += trees[i].team == team ? -1 : 1;
        }


        float close = Controller.c.bodyRadius + Controller.c.bs + GameConstants.BULLET_SPAWN_OFFSET;
        // closest unit/tree if it is an enemy and whether the path is clear
        BodyInfo[] closest = new BodyInfo[GRANULARITY];
        boolean[] clear = new boolean[GRANULARITY];
        for (int i = 0; i < GRANULARITY; i++) {
            RobotInfo unit = closestUnits[i];
            TreeInfo tree = closestTrees[i];
            BodyInfo info = null;
            boolean mustHit = false;
            // override clear if archon and we don't want to shoot archons
            boolean clearOverride = true;
            if (unit == null) {
                if (tree != null && tree.team == enemy) {
                    info = tree;
                }
            } else if (tree == null) {
                if (unit.team == enemy) {
                    info = unit;
                    if (!shouldHitArchon && unit.type == RobotType.ARCHON) {
                        clearOverride = false;
                    }
                }
            } else {
                if (here.distanceTo(unit.location) - unit.getRadius() <= here.distanceTo(tree.location) - tree.radius) {
                    if (unit.team == enemy) {
                        info = unit;
                        if (!shouldHitArchon && unit.type == RobotType.ARCHON) {
                            clearOverride = false;
                        }
                    }
                } else {
                    if (tree.team == enemy) {
                        info = tree;
                    } else if (tree.team == Team.NEUTRAL && unit.team == enemy) {
                        info = unit;
                        mustHit = true;
                    }
                }
            }

            if (info != null) {
                closest[i] = info;
                float total = here.distanceTo(info.getLocation());
                float distance = total - info.getRadius() - OtherGameConstants.EPSILON;

                if (total < close) {
                    clear[i] = clearOverride && checkPathClearClose(distance, here.directionTo(info.getLocation()));
                } else {
                    clear[i] = clearOverride && checkPathClearFar(distance, here.directionTo(info.getLocation()));
                }

                if (mustHit && !clear[i]) {
                    closest[i] = null;
                }
            }
        }

        AttackInfo best = null;
        int attackScore = 0;
        boolean canTriad = rc.canFireTriadShot();
        boolean canPentad = rc.canFirePentadShot();
        for (int i = 0; i < GRANULARITY; i++) {
            if (!clear[i]) {
                continue;
            }

            int right = (i - 1 + GRANULARITY) % GRANULARITY;
            int left = (i + 1) % GRANULARITY;
            boolean goodLeft = clear[left] || (closest[left] != null && score[left] > 0);
            boolean goodRight = clear[right] || (closest[right] != null && score[right] > 0);
            if (!canTriad) {
                int s = score[i] + 100;
                if (attackScore < s) {
                    attackScore = s;
                    best = new AttackInfo(AttackType.SINGLE, here.directionTo(closest[i].getLocation()));
                }
            } else if (goodLeft && goodRight) {
                int s = score[left] + score[right] + score[i] + 300;
                if (attackScore < s) {
                    attackScore = s;
                    AttackType type = canPentad ? AttackType.PENTAD : AttackType.TRIAD;
                    best = new AttackInfo(type, here.directionTo(closest[i].getLocation()));
                }
            } else if (goodLeft) {
                int s = score[left] + score[i] + 200;
                if (attackScore < s) {
                    attackScore = s;
                    AttackType type = canPentad ? AttackType.PENTAD : AttackType.TRIAD;
                    best = new AttackInfo(type, Math2.dirAvg(here, closest[i], closest[left]));
                }
            } else if (goodRight) {
                int s = score[left] + score[i] + 200;
                if (attackScore < s) {
                    attackScore = s;
                    AttackType type = canPentad ? AttackType.PENTAD : AttackType.TRIAD;
                    best = new AttackInfo(type, Math2.dirAvg(here, closest[i], closest[right]));
                }
            } else {
                int s = score[i] + 100;
                if (attackScore < s) {
                    attackScore = s;
                    MapLocation loc = closest[i].getLocation();
                    float distance = here.distanceTo(loc);
                    boolean triad = false;
                    boolean pentad = false;
                    if (closest[i] instanceof RobotInfo) {
                        RobotInfo info = (RobotInfo) closest[i];
                        // not going to hit
                        if (distance - close > info.getRadius() - info.type.strideRadius) {
                            if (hasAllies) {
                                pentad = true;
                            }
                            triad = true;
                        }
                    }

                    if (StrictMath.asin(closest[i].getRadius() / distance)
                            > StrictMath.toRadians(GameConstants.TRIAD_SPREAD_DEGREES)) {
                        // shotgun face
                        triad = true;
                    }
                    AttackType type = triad ? AttackType.TRIAD : AttackType.SINGLE;
                    if (pentad && canPentad) {
                        type = AttackType.PENTAD;
                    }

                    best = new AttackInfo(type, here.directionTo(loc));
                }
            }
        }
        return best;
    }

    // for scouts.
    public AttackInfo attackSingle() throws GameActionException {
        RobotController rc = Controller.crc;
        if (!rc.canFireSingleShot()) {
            return null;
        }

        float close = Controller.c.bodyRadius + Controller.c.bs + GameConstants.BULLET_SPAWN_OFFSET;
        RobotInfo[] nearbyEnemies = rc.senseNearbyRobots(close, Controller.c.enemy);
        MapLocation here = rc.getLocation();

        AttackInfo bestAttack = null;
        int length = Math.min(nearbyEnemies.length, 16);
        for (int i = 0; i < length; i++) {
            RobotInfo info = nearbyEnemies[i];
            MapLocation loc = info.location;
            // DON'T SHOOT ANYTHING EXCEPT GARDENERS, AND SCOUTS
            switch (info.type) {
                case GARDENER:
                case SCOUT:
                    break;
                default:
                    continue;
            }
            float distance = here.distanceTo(loc) - info.getRadius();
            // they can dodge
            if (distance - close > info.getRadius() - info.type.strideRadius) {
                continue;
            }

            if (!checkPathClearClose(distance, here.directionTo(loc))) {
                continue;
            }

            if (info.type == RobotType.GARDENER) {
                return new AttackInfo(AttackType.SINGLE, here.directionTo(loc));
            }

            bestAttack = new AttackInfo(AttackType.SINGLE, here.directionTo(loc));
        }

        return bestAttack;
    }

    public boolean checkPathClearFar(float distance, Direction dir) throws GameActionException {
        RobotController rc = Controller.crc;
        MapLocation here = rc.getLocation();
        float BULLET_CHECK_INTERVAL = Attack.BULLET_CHECK_INTERVAL;
        float base = Controller.c.bodyRadius + GameConstants.BULLET_SPAWN_OFFSET;
        // loop vars
        float end = StrictMath.min(distance, MAX_DIST);
        for (float dist = base; dist < end; dist += BULLET_CHECK_INTERVAL) {
            rc.setIndicatorDot(here.add(dir, dist), 0, 0, 0);
            if (rc.isLocationOccupied(here.add(dir, dist))) {
                return false;
            }
        }

        return true;
    }

    // distance is from your center to their edge.
    public boolean checkPathClearClose(float distance, Direction dir) throws GameActionException {
        RobotController rc = Controller.crc;
        MapLocation here = rc.getLocation();
        float INTERVAL = Attack.BULLET_CHECK_INTERVAL_CLOSE;
        float base = Controller.c.bodyRadius + GameConstants.BULLET_SPAWN_OFFSET;

        // loop vars
        for (float dist = base; dist < distance; dist += INTERVAL) {
            rc.setIndicatorDot(here.add(dir, dist), 0, 0, 0);
            if (rc.isLocationOccupied(here.add(dir, dist))) {
                return false;
            }
        }

        return true;
    }
}
