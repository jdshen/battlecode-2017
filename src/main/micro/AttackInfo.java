package main.micro;

import battlecode.common.Direction;

public strictfp class AttackInfo {
    public final AttackType type;
    public final Direction dir;

    public AttackInfo(AttackType type, Direction dir) {
        this.type = type;
        this.dir = dir;
    }
}
