package main.micro;

import battlecode.common.MapLocation;

public class DamageCache {
    public int index;
    public MapLocation[] locs;
    public float[] dmg;
    private static final int MAX_LOCS = 50;
    private static final float MAX_DAMAGE = 10000;


    public void update() {
        index = 0;
        locs = new MapLocation[MAX_LOCS];
        dmg = new float[MAX_LOCS];
    }

    public int minDamageIndex(int start) {
        int minIndex = -1;
        float min = MAX_DAMAGE;
        float[] cache = this.dmg;
        int index = this.index;

        for (int i = start; i < index; i++) {
            if (cache[i] < min) {
                min = cache[i];
                minIndex = i;
            }
        }

        return minIndex;
    }

    public void store(MapLocation loc, float damage) {
        if (index < MAX_LOCS) {
            locs[index] = loc;
            dmg[index] = damage;
            index++;
        }
    }
}
