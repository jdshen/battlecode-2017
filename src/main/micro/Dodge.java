package main.micro;

import battlecode.common.*;
import main.controllers.Controller;

public strictfp class Dodge {
    public final int MAX_BULLETS_TO_CONSIDER = Controller.c.type == RobotType.SCOUT ? 5 : 9; // ~50 bytecode per bullet per location
    public final int MAX_UPDATE_BULLETS = Controller.c.type == RobotType.SCOUT ? 12 : 24;


    // keep list of bullets that need checking
    private int bullets = 0;
    private BulletInfo[] bulletInfos;

    // Update internal state for this round. should always be called before use
    public void update() {
        RobotController rc = Controller.crc;
        Controller c = Controller.c;
        BulletInfo[] nearbyBullets = rc.senseNearbyBullets();
        MapLocation here = rc.getLocation();
        float radius = c.bodyRadius + c.stride;

        int MAX_BULLETS_TO_CONSIDER = this.MAX_BULLETS_TO_CONSIDER;
        BulletInfo[] relevantBullets = new BulletInfo[MAX_BULLETS_TO_CONSIDER];
        int numBullets = 0;

        // prune out things that wont hit stride range
        int length = Math.min(nearbyBullets.length, MAX_UPDATE_BULLETS);
        for (int i = 0; i < length; i++) {
            if (intersects(nearbyBullets[i], here, radius)) {
                relevantBullets[numBullets++] = nearbyBullets[i];
                if (numBullets >= MAX_BULLETS_TO_CONSIDER) {
                    break;
                }
            }
        }

        bullets = numBullets;
        bulletInfos = relevantBullets;
    }

    public float damageAtLoc(MapLocation loc) {
        Controller c = Controller.c;
        float bodyRadius = c.bodyRadius;
        float damage = 0;
        BulletInfo[] bulletInfos = this.bulletInfos;
        for (int i = bullets; --i >=0;) {
            damage += intersectDamage(bulletInfos[i], loc, bodyRadius);
        }
//        Controller.crc.setIndicatorDot(loc, 255, 255 - (int) (damage * 25), 255 - (int) (damage * 25));

        return damage;
    }

    /**
     * helper function, roughly checks whether a bullet will intersect with a circle at x, y with given radius.
     * Assumes the bullet is an infinite line
     */
    private boolean intersects(BulletInfo bullet, MapLocation loc, float radius) {
        float distance = bullet.location.distanceTo(loc);
        // bullet is inside circle
        if (distance <= radius) {
            return true;
        }

        // calculate angle range where you get hit
        double maxAngle = StrictMath.asin(radius/distance);
        float radsBetween = bullet.location.directionTo(loc).radiansBetween(bullet.dir);
        return StrictMath.abs(radsBetween) <= maxAngle;
    }

    /**
     * helper function, estimates damage of a bullet hitting a circle at x, y with given radius.
     * Assumes the bullet is an infinite line, but weights bullets by distance to center if they are farther away
     */
    private float intersectDamage(BulletInfo bullet, MapLocation loc, float radius) {
        float distance = bullet.location.distanceTo(loc);
        // bullet is inside circle
        if (distance <= radius) {
            return bullet.damage;
        }

        // calculate angle range where you get hit
        float maxAngle = (float) StrictMath.asin(radius/distance);
        float radsBetween = StrictMath.abs(bullet.location.directionTo(loc).radiansBetween(bullet.dir));
        if (radsBetween > maxAngle) {
            return 0;
        }

        // if not going to hit this turn, weight the damage less
        if (distance - bullet.speed > radius) {
            // Hopefully nothing weird casting this
            return (1 - (float) (StrictMath.sin(radsBetween) * distance / radius)) * bullet.damage;
        }

        return bullet.damage;
    }
}
