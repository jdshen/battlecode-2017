package main.helper;

import battlecode.common.MapLocation;
import battlecode.common.TreeInfo;
import main.controllers.Controller;
import main.util.FastIdLocationMap;
import main.util.Math2;
import main.util.OtherGameConstants;

/**
 * Tracks close by and other trees
 */
public class NeutralTreeTracker {
    private FastIdLocationMap map;
    public NeutralTreeTracker() {
        map = new FastIdLocationMap();
    }

    // returns closest TreeInfo
    public TreeInfo update(TreeInfo[] trees) {
        FastIdLocationMap map = this.map;
        TreeInfo closest = null;
        float best = OtherGameConstants.MAX_DIST;
        MapLocation here = Controller.crc.getLocation();

        if (map.size() > 0) {
            for (int i = Math2.min(trees.length, 16); --i >= 0;) {
                TreeInfo tree = trees[i];
                if (tree.containedBullets > 0) {
                    float dist = tree.location.distanceTo(here) - tree.radius;
                    if (dist < best) {
                        best = dist;
                        closest = tree;
                    }

                    if (!map.contains(tree.ID)) {
                        map.add(tree.ID, tree.location);
                    }
                } else {
                    map.remove(tree.ID);
                }
            }
        } else {
            for (int i = Math2.min(trees.length, 16); --i >= 0;) {
                TreeInfo tree = trees[i];
                if (tree.containedBullets > 0) {
                    float dist = tree.location.distanceTo(here) - tree.radius;
                    if (dist < best) {
                        best = dist;
                        closest = tree;
                    }

                    map.add(tree.ID, tree.location);
                }
            }
        }

        return closest;
    }

    // pops a neutral tree that hasnt been collected yet
    public MapLocation pop() {
        return map.pop();
    }
}
