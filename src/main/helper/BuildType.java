package main.helper;

import battlecode.common.RobotController;
import battlecode.common.RobotType;

public enum BuildType {
    TREE(),
    ARCHON(RobotType.ARCHON),
    GARDENER(RobotType.GARDENER),
    LUMBERJACK(RobotType.LUMBERJACK),
    SCOUT(RobotType.SCOUT),
    SOLDIER(RobotType.SOLDIER),
    TANK(RobotType.TANK);

    public final RobotType type;
    public final boolean tree;
    BuildType(RobotType type) {
        this.type = type;
        this.tree = false;
    }

    BuildType() {
        this.type = null;
        this.tree = true;
    }

    boolean isParentRobot(RobotType parent) {
        if (type != null) {
            return this.type.spawnSource == parent;
        }

        return parent == RobotType.GARDENER;
    }

    boolean canBuild(RobotController rc) {
        if (this.type != null) {
            return rc.hasRobotBuildRequirements(type);
        } else {
            return rc.hasTreeBuildRequirements();
        }
    }

    public static BuildType getBuildType(RobotType type) {
        switch (type) {
            case ARCHON:
                return BuildType.ARCHON;
            case GARDENER:
                return BuildType.GARDENER;
            case LUMBERJACK:
                return BuildType.LUMBERJACK;
            case SOLDIER:
                return BuildType.SOLDIER;
            case TANK:
                return BuildType.TANK;
            case SCOUT:
                return BuildType.SCOUT;
        }

        // should never get here
        return null;
    }
}
