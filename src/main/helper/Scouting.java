package main.helper;

import battlecode.common.*;
import main.controllers.Controller;
import main.nav.Move;
import main.util.FastLocRoundSet;
import main.util.OtherGameConstants;

public class Scouting {
    private FastLocRoundSet blocks = new FastLocRoundSet((int) Controller.c.sr);
    private NeutralTreeTracker tracker = new NeutralTreeTracker();
    private Direction last;
    private MapLocation lastLoc;
    public Scouting() {
        MapLocation[] locs = Controller.crc.getInitialArchonLocations(Controller.c.enemy);
        last = Controller.crc.getLocation().directionTo(locs[0]);
        lastLoc = null;
    }

    public MapLocation scout() throws GameActionException {
        // localize
        RobotController rc = Controller.crc;
        Controller c = Controller.c;
        FastLocRoundSet blocks = this.blocks;
        MapLocation here = rc.getLocation();
        int roundNum = rc.getRoundNum();

        TreeInfo[] trees = rc.senseNearbyTrees(-1, Team.NEUTRAL);

        this.blocks.add(here, roundNum);
        TreeInfo closest = tracker.update(trees);
        if (closest != null) {
            return closest.location;
        }

        // find place
        float radius = c.sr - OtherGameConstants.EPSILON;

        MapLocation to = lastLoc;
        int min = to == null ? rc.getRoundLimit() : blocks.get(to);
        Direction d = last;
        for (int i = 8; --i >= 0; d = d.rotateLeftDegrees(45)) {
            MapLocation loc = here.add(d, radius);
            if (!rc.onTheMap(loc)) {
                // so that if we leave, we dont try to go somewhere near edge of map again
                blocks.add(loc, roundNum);
                continue;
            }

            int round = blocks.get(loc);
            if (round < min) {
                to = loc;
                min = round;
            }
        }

        if (to != null) {
            lastLoc = to;
            last = here.directionTo(to);
            return to;
        }

        // should never reach here
        return null;
    }
}
