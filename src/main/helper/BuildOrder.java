package main.helper;

import battlecode.common.*;
import main.controllers.Controller;
import main.util.OtherGameConstants;

public class BuildOrder {
    private static final BuildType[] AGG_ORDER = new BuildType[]{
        BuildType.GARDENER, BuildType.SOLDIER, BuildType.SOLDIER,
    };

    private static final BuildType[] MID_ORDER = new BuildType[] {
        BuildType.GARDENER, BuildType.SCOUT, BuildType.SOLDIER,
    };

    private static final BuildType[] STD_ORDER = new BuildType[]{
        BuildType.GARDENER, BuildType.SCOUT, BuildType.LUMBERJACK
    };

    private static final float AGG = 25;
    private static final float MID = 50;
    private static final int LARGE_ROBOT_COUNT = 75;

    private int step;
    private BuildType[] order;
    private int[] orderCounts;

    public BuildOrder() {
        step = 0;
        // Note: if init conditions are not stable, then make sure to write this to a channel
        // so the next bot does the same thing
        order = getOrder();
        orderCounts = new int[OtherGameConstants.NUM_BUILD_TYPES];
    }

    private BuildType[] getOrder() {
        RobotController rc = Controller.crc;
        MapLocation[] a = rc.getInitialArchonLocations(Controller.c.team);
        MapLocation[] b = rc.getInitialArchonLocations(Controller.c.enemy);
        float closest = OtherGameConstants.MAX_DIST;
        for (int i = a.length; --i >= 0;) {
            for (int j = b.length; --j >= 0;) {
                float dist = a[i].distanceTo(b[i]);
                if (dist < closest) {
                    closest = dist;
                }
            }
        }

        if (closest <= AGG) {
            return AGG_ORDER;
        }

        if (closest <= MID) {
            return MID_ORDER;
        }

        return STD_ORDER;
    }

    private BuildType start(int[] buildCount) {
        BuildType build = order[step];
        int id = build.ordinal();
        int count = orderCounts[id];
        if (buildCount[id] <= count) {
            if (build.isParentRobot(Controller.c.type)) {
                return build;
            }

            return null;
        }

        step++;
        orderCounts[id]++;
        return null;
    }

    public BuildType next(int[] buildCount, int[] unitCounts, int trees) {
        if (unitCounts == null || buildCount == null) {
            return null;
        }

        if (step < order.length && Controller.crc.getRoundNum() <= 200) {
            return start(buildCount);
        }

        RobotController rc = Controller.crc;
        Controller c = Controller.c;

        if (Controller.c.type == RobotType.ARCHON) {
            if ((trees >= unitCounts[RobotType.GARDENER.ordinal()] * 3 ||
                    trees >= 1 && unitCounts[RobotType.GARDENER.ordinal()] == 1)
                    && BuildType.GARDENER.canBuild(rc)) {
                return BuildType.GARDENER;
            }
            return null;
        }

        // otherwise you're a gardener
        int robots = rc.getRobotCount();
        TreeInfo[] allyTrees = rc.senseNearbyTrees(c.bodyRadius + GameConstants.BULLET_TREE_RADIUS, c.team);
        if (robots >= LARGE_ROBOT_COUNT) {
            // only consider building a tree to win
            if (allyTrees.length <= 4 && BuildType.TREE.canBuild(rc)) {
                return BuildType.TREE;
            }

            return null;
        } else {
            // arbitrary
            float bulletUsage = unitCounts[RobotType.SOLDIER.ordinal()] * 1.5f +
                unitCounts[RobotType.TANK.ordinal()] * 2.0f +
                unitCounts[RobotType.LUMBERJACK.ordinal()] * 0.8f +
                unitCounts[RobotType.SCOUT.ordinal()] * 1.2f;

            if (trees <= bulletUsage && allyTrees.length <= 3 &&
                    rc.getTeamBullets() < bulletUsage * 10 + RobotType.TANK.bulletCost) {
                return BuildType.TREE;
            }

            if (BuildType.TANK.canBuild(rc)) {
                return BuildType.TANK;
            }

            int smallRobots = unitCounts[RobotType.SOLDIER.ordinal()] +
                unitCounts[RobotType.LUMBERJACK.ordinal()];
            if (unitCounts[RobotType.SCOUT.ordinal()] <= 3 &&
                    unitCounts[RobotType.SCOUT.ordinal()] <= smallRobots / 6) {
                if (BuildType.SCOUT.canBuild(rc)) {
                    return BuildType.SCOUT;
                }
            }

            if (unitCounts[RobotType.SOLDIER.ordinal()] <= 4 * smallRobots / 6) {
                if (BuildType.SOLDIER.canBuild(rc)) {
                    return BuildType.SOLDIER;
                }
            }

            if (unitCounts[RobotType.LUMBERJACK.ordinal()] <= 2 * smallRobots / 6) {
                if (BuildType.LUMBERJACK.canBuild(rc)) {
                    return BuildType.LUMBERJACK;
                }
            }
            return null;
        }
    }
}
