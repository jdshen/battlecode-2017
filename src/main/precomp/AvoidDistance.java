package main.precomp;

import battlecode.common.GameConstants;
import battlecode.common.RobotType;
import main.util.OtherGameConstants;

public class AvoidDistance {
    public static void main(String[] args) {
        printSwitchCase();
    }

    public static void printSwitchCase() {
        System.out.println("switch (me) {");
        for (RobotType me : RobotType.values()) {
            System.out.println("case " + me.name() + ":");
            System.out.println("switch(them) {");
            for (RobotType them : RobotType.values()) {
                System.out.println("case " + them.name() + ":");
                if (them.attackPower < OtherGameConstants.DELTA) {
                    System.out.println("return 0.0f;");
                    continue;
                }

                if (me == RobotType.SCOUT) {
                    if (them == RobotType.LUMBERJACK) {
                        System.out.println("return " + (GameConstants.LUMBERJACK_STRIKE_RADIUS + them.strideRadius + them.bodyRadius + me.bodyRadius) + "f" + ";");
                    } else {
                        System.out.println("return " + (-me.strideRadius + them.strideRadius + 7.662f) + "f" + ";");
                    }

                    continue;
                }

                float atk = (them == RobotType.LUMBERJACK) ? GameConstants.LUMBERJACK_STRIKE_RADIUS :
                    them.bulletSpeed - me.strideRadius;

                System.out.println("return " + (me.bodyRadius + atk + them.strideRadius + them.bodyRadius) + "f" + ";");
            }
            System.out.println("default:");
            System.out.println("return 0;");
            System.out.println("}");
        }
        System.out.println("}");
        System.out.println("return 0;");
    }
}
