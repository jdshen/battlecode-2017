package main.util;

import battlecode.common.Direction;
import battlecode.common.MapLocation;
import org.junit.Test;

import static org.junit.Assert.*;

public class FastLocDirSetTest {
    @Test
    public void add() throws Exception {
        FastLocDirSet set = new FastLocDirSet();
        MapLocation loc = new MapLocation(1, 2);

        for (float i = -180; i <= 180; i+= 0.5) {
            set.clear();
            Direction dir = new Direction((float)Math.toRadians(i));
            assertFalse(set.contains(loc, dir));
            set.add(loc, dir);
            assertTrue(set.contains(loc, dir));
        }
    }

}